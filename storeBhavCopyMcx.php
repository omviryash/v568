<?php
  include "etc/om_config.inc";
  session_start();

  $smarty = new SmartyWWW();

  $filePath = "./bhavcopies";
  $fileName = $_SESSION['fileName'];

  $generalQuery = "SELECT * FROM general";
  $generalResult = mysql_query($generalQuery);
  if($generalRow = mysql_fetch_array($generalResult))
  {
    $fileName = $generalRow['fileName'];
  }

  $row = 0;
  $scLineCount = 1;
  $FOLineCount = 1;
  $NSELineCount = 1;
  $handle = fopen($filePath."/".$fileName, "r");
  while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
  {
    if(isset($_GET['exc']) && $_GET['exc']=="MCX")
    {
    	$num = count($data);
    	$exchange					=	'MCX';
    	$expiryDate				= "";
    	$bhavcopyDate 		= $data[0];
    	$sessionId 				= $data[1];
    	$marketType 			= $data[2];
    	$instrumentId 		= $data[3];
    	$instrumentName 	= $data[4];
    	$contractCode 		= $data[5];
    	$expiryDateBc 		= $data[6];
    	$strikePrice 			= $data[7];
    	$optionType 			= $data[8];
    	$previousClosePrice 		= $data[9];
    	$openPrice 				= $data[10];
    	$highPrice 				= $data[11];
    	$lowPrice 				= $data[12];
    	$closePrice 			= $data[13];
    	$totalQtyTrade 		= $data[14];
    	$totalValueTrade 	= $data[15];
    	$lifeHigh 				= $data[16];
    	$lifeLow 					= $data[17];
    	$quoteUnits 			= $data[18];
    	$settlementPrice 	= $data[19];
    	$noOfTrades 			= $data[20];
    	$openInterest 		= $data[21];
    	$avgTradePrice 		= $data[22];
    	$row++;
    	
   		$monthStr = substr($data[0],2,3);
    	switch ($monthStr)
    	{
    	  case "JAN":
    	    $monthInt = '01';
    	    break;
    	  case "FEB":
    	    $monthInt = '02';
    	    break;
    	  case "MAR":
    	    $monthInt = '03';
    	    break;
    	  case "APR":
    	    $monthInt = '04';
    	    break;
    	  case "MAY":
    	    $monthInt = '05';
    	    break;
    	  case "JUN":
    	    $monthInt = '06';
    	    break;
    	  case "JUL":
    	    $monthInt = '07';
    	    break;
    	  case "AUG":
    	    $monthInt = '08';
    	    break;
    	  case "SEP":
    	    $monthInt = '09';
    	    break;
    	  case "OCT":
    	    $monthInt = '10';
    	    break;
    	  case "NOV":
    	    $monthInt = '11';
    	    break;
    	  case "DEC":
    	    $monthInt = '12';
    	    break;
    	}
    	
   		$bhavcopyDate = substr($data[0],5,4)."-".$monthInt."-".substr($data[0],0,2);
////	////////For Expiry:Start
   		if(strlen(trim($data[6])) > 0)
   		{
	  	 	$monthStrExp = substr($data[6],2,3);
	  	  switch ($monthStrExp)
	  	  {
	  	    case "JAN":
	  	      $monthIntExp = '01';
	  	      break;
	  	    case "FEB":
	  	      $monthIntExp = '02';
	  	      break;
	  	    case "MAR":
	  	      $monthIntExp = '03';
	  	      break;
	  	    case "APR":
	  	      $monthIntExp = '04';
	  	      break;
	  	    case "MAY":
	  	      $monthIntExp = '05';
	  	      break;
	  	    case "JUN":
	  	      $monthIntExp = '06';
	  	      break;
	  	    case "JUL":
	  	      $monthIntExp = '07';
	  	      break;
	  	    case "AUG":
	  	      $monthIntExp = '08';
	  	      break;
	  	    case "SEP":
	  	      $monthIntExp = '09';
	  	      break;
	  	    case "OCT":
	  	      $monthIntExp = '10';
	  	      break;
	  	    case "NOV":
	  	      $monthIntExp = '11';
	  	      break;
	  	    case "DEC":
	  	      $monthIntExp = '12';
	  	      break;
	  	  }
	  	
	  	  $expiryDate = substr($data[6],5,4)."-".$monthIntExp."-".substr($data[6],0,2);
    		//echo $expiryDate;
    	}
////	////////For Expiry:End
 			$insertDenied=0;
 			$noDuplication ="SELECT contractCode FROM bhavcopy WHERE bhavcopyDate='".$bhavcopyDate."' AND contractCode='".$contractCode."' AND 
 			                  expiryDate='".$expiryDate."'";
			$resultNoDuplicate = mysql_query($noDuplication); 
			if($resultNoDuplicate && mysql_num_rows($resultNoDuplicate)!=0)
			{
				$insertDenied = '1';
			}
			if($highPrice == 0)
			{
			  $insertDenied = '1';
			}
			if($insertDenied == '0')
			{    
				 $insertData = "INSERT INTO bhavcopy (exchange,bhavcopyDate,sessionId,marketType,instrumentId,instrumentName,
				 																		contractCode,expiryDate,expiryDateBc,strikePrice,optionType,previousClosePrice,openPrice,highPrice,
				 																		lowPrice,closePrice,totalQtyTrade,totalValueTrade,lifeHigh,lifeLow,quoteUnits,settlementPrice,
				                                     noOfTrades,openInterest,avgTradePrice)
				                                      	VALUES ('".$exchange."','".$bhavcopyDate."','".$sessionId."','".$marketType."','".$instrumentId."',
				 																						'".$instrumentName."','".$contractCode."','".$expiryDate."','".$expiryDateBc."',
				 																						'".$strikePrice."','".$optionType."','".$previousClosePrice."','".$openPrice."',
				 																						'".$highPrice."','".$lowPrice."','".$closePrice."','".$totalQtyTrade."',
				 																						'".$totalValueTrade ."','".$lifeHigh."','".$lifeLow."','".$quoteUnits."',
				 																						'".$settlementPrice."','".$noOfTrades."','".$openInterest."','".$avgTradePrice."')";
				 //echo $insertData;
				 $resultData = mysql_query($insertData);					
			}
		}
		elseif(isset($_GET['exc']) && $_GET['exc'] == 'BSEC')
		{
			$exchange					=	'BSE CASH';
			$scCode 					= $data[0];
			$scName 					= $data[1];
			$scGroup 					= $data[2];
			$scType 					= $data[3];
			$scOpenPrice			= $data[4];
			$scHighprice			= $data[5];
			$scLowPrice 			= $data[6];
			$scClosePrice			= $data[7];
			$scLastPrice			= $data[8];
			$scPrevClosePrice	= $data[9];
			$scNoOfTrade			= $data[10];
			$scQtyTrade				= $data[11];
			$scValueTrade			= $data[12];
			$scTDLC						= $data[13];
			
			$bhavcopyDate =  "20".substr($fileName,6,2)."-".substr($fileName,4,2)."-".substr($fileName,2,2);
//			echo $bhavcopyDate;
			$insertData ="";
			$insertDenied ='0';
			$noDuplication ="SELECT contractCode FROM bhavcopy WHERE bhavcopyDate='".$bhavcopyDate."' AND contractCode='".$scName."'";
			$resultNoDuplicate = mysql_query($noDuplication); 
			if($resultNoDuplicate && mysql_num_rows($resultNoDuplicate)!=0)
			{
				$insertDenied = '1';
			}
			if($scHighprice == 0)
			{
			  $insertDenied ='1';
			}
			
			if($insertDenied == '0')
			{
				$insertData = "INSERT INTO bhavcopy (exchange,bhavcopyDate,scriptCode,contractCode,scriptGroup,scriptType,
																							openPrice,highPrice,lowPrice,closePrice,lstTradePrice,previousClosePrice,
																							noOfTrades,totalQtyTrade,totalValueTrade,tdcl) VALUES
																							('".$exchange."','".$bhavcopyDate."','".$scCode."','".$scName."','".$scGroup."',
																							'".$scType."','".$scOpenPrice."','".$scHighprice."','".$scLowPrice."',
																							'".$scClosePrice."','".$scLastPrice."','".$scPrevClosePrice."','".$scNoOfTrade."',
																							'".$scQtyTrade."','".$scValueTrade."','".$scTDLC."')";
				if($scLineCount > 1)
				  $resultData = mysql_query($insertData);
			}
			$scLineCount++;
			
			
		}
    elseif(isset($_GET['exc']) && $_GET['exc'] == 'NSEC')
    {
      $exchange         = 'NSE CASH';
    	$itemName      		= $data[0];
    	$instrumentName   = $data[1];
    	$openPrice     		= $data[2];
    	$highPrice   			= $data[3];
    	$lowPrice    			= $data[4];
    	$closePrice  			= $data[5];
    	$lastTradePrice 	= $data[6];
    	$previousClosePrice= $data[7];
    	$totalQtyTrade  	= $data[8];
    	$totalValueTrade	= $data[9];
    	$bhavcopyDate   	= $data[10];
   		
   		$monthStr = substr($data[10],3,3);
   		$monthInt ="";
    	switch ($monthStr)
    	{
    	  case "JAN":
    	    $monthInt = '01';
    	    break;
    	  case "FEB":
    	    $monthInt = '02';
    	    break;
    	  case "MAR":
    	    $monthInt = '03';
    	    break;
    	  case "APR":
    	    $monthInt = '04';
    	    break;
    	  case "MAY":
    	    $monthInt = '05';
    	    break;
    	  case "JUN":
    	    $monthInt = '06';
    	    break;
    	  case "JUL":
    	    $monthInt = '07';
    	    break;
    	  case "AUG":
    	    $monthInt = '08';
    	    break;
    	  case "SEP":
    	    $monthInt = '09';
    	    break;
    	  case "OCT":
    	    $monthInt = '10';
    	    break;
    	  case "NOV":
    	    $monthInt = '11';
    	    break;
    	  case "DEC":
    	    $monthInt = '12';
    	    break;
    	}
   		$bhavcopyDate = substr($data[10],7,4)."-".$monthInt."-".substr($data[10],0,2);
			$insertDenied ='0';
			$noDuplication ="SELECT contractCode FROM bhavcopy WHERE bhavcopyDate='".$bhavcopyDate."' AND contractCode='".$itemName."'";
			$resultNoDuplicate = mysql_query($noDuplication); 
			if($resultNoDuplicate && mysql_num_rows($resultNoDuplicate)!=0)
			{
				$insertDenied = '1';
			}

    	if($insertDenied == 0)
    	{  
    	  $insertData = "INSERT INTO bhavcopy (exchange,bhavcopyDate,instrumentName,
			    	 																		contractCode,openPrice,highPrice,
			    	 																		lowPrice,closePrice,previousClosePrice,totalQtyTrade,
			    	 																		totalValueTrade,lstTradePrice)
			    	                                      	VALUES ('".$exchange."','".$bhavcopyDate."','".$instrumentName."',
			    	 																						'".$itemName."','".$openPrice."','".$highPrice."',
			    	 																						'".$lowPrice."','".$closePrice."','".$previousClosePrice."','".$totalQtyTrade."',
			    	 																						'".$totalValueTrade ."','".$lastTradePrice."')";
    	  if($NSELineCount > 1)
    	    $resultData = mysql_query($insertData);
    	}
    	$NSELineCount++;
    }
    elseif(isset($_GET['exc']) && $_GET['exc'] == 'FO')
    {
      $exchange         = 'FutureOptions';
      $instrumentId 		= $data[0];
    	$itemName      		= $data[1];
    	$expiryDate 			= $data[2];
    	$strikePrice     	= $data[3];
    	$optionType    		= $data[4];
    	$openPrice     		= $data[5];
    	$highPrice   			= $data[6];
    	$lowPrice    			= $data[7];
    	$closePrice  			= $data[8];
    	$settlementPrice	= $data[9];
    	$totalQtyTrade		= $data[10];
    	$totalValueTrade	= $data[11];
    	$openInterest			= $data[12];
    	$cngOpenInterest	= $data[13];
    	$bhavcopyDate			= $data[14];
////For BhavCopyDate:START

   		$monthInt="";
   		$monthStr = substr($data[14],3,3);
    	switch ($monthStr)
    	{
    	  case "JAN":
    	    $monthInt = '01';
    	    break;
    	  case "FEB":
    	    $monthInt = '02';
    	    break;
    	  case "MAR":
    	    $monthInt = '03';
    	    break;
    	  case "APR":
    	    $monthInt = '04';
    	    break;
    	  case "MAY":
    	    $monthInt = '05';
    	    break;
    	  case "JUN":
    	    $monthInt = '06';
    	    break;
    	  case "JUL":
    	    $monthInt = '07';
    	    break;
    	  case "AUG":
    	    $monthInt = '08';
    	    break;
    	  case "SEP":
    	    $monthInt = '09';
    	    break;
    	  case "OCT":
    	    $monthInt = '10';
    	    break;
    	  case "NOV":
    	    $monthInt = '11';
    	    break;
    	  case "DEC":
    	    $monthInt = '12';
    	    break;
    	}
    	
   		$bhavcopyDate = substr($data[14],7,4)."-".$monthInt."-".substr($data[14],0,2);
    	
////For BhavCopyDate:END

////For ExpiryDate:START

   		$monthStr = substr($data[2],3,3);
    	switch ($monthStr)
    	{
    	  case "Jan":
    	    $monthInt = '01';
    	    break;
    	  case "Feb":
    	    $monthInt = '02';
    	    break;
    	  case "Mar":
    	    $monthInt = '03';
    	    break;
    	  case "Apr":
    	    $monthInt = '04';
    	    break;
    	  case "May":
    	    $monthInt = '05';
    	    break;
    	  case "Jun":
    	    $monthInt = '06';
    	    break;
    	  case "Jul":
    	    $monthInt = '07';
    	    break;
    	  case "Aug":
    	    $monthInt = '08';
    	    break;
    	  case "Sep":
    	    $monthInt = '09';
    	    break;
    	  case "Oct":
    	    $monthInt = '10';
    	    break;
    	  case "Nov":
    	    $monthInt = '11';
    	    break;
    	  case "Dec":
    	    $monthInt = '12';
    	    break;
    	}
    	
   		$expiryDate = substr($data[2],7,4)."-".$monthInt."-".substr($data[2],0,2);
      $expiryDateBc = substr($data[2],0,2).strtoupper(substr($data[2],3,3)).substr($data[2],7,4);
////For ExpiryDate:END
 			$insertDenied=0;
 			$noDuplication ="SELECT contractCode FROM bhavcopy WHERE bhavcopyDate='".$bhavcopyDate."' AND contractCode='".$itemName."' AND 
 			                  expiryDate='".$expiryDate."'";
			$resultNoDuplicate = mysql_query($noDuplication); 
			if($resultNoDuplicate && mysql_num_rows($resultNoDuplicate)!=0)
			{
				$insertDenied = '1';
			}
			if($highPrice == 0 || $optionType != 'XX')
			{
			  $insertDenied = '1';
			}
      if($insertDenied == 0)
    	{
    	  $insertData = "INSERT INTO bhavcopy (exchange,bhavcopyDate,instrumentName,
			  	 																		contractCode,expiryDate,expiryDateBc,strikePrice,
			  	 																		optionType,openPrice,highPrice,
			  	 																		lowPrice,closePrice,totalQtyTrade,
			  	 																		totalValueTrade,settlementPrice,openInterest)
			  	                                      	VALUES ('".$exchange."','".$bhavcopyDate."','".$instrumentId."',
			  	 																						'".$itemName."','".$expiryDate."','".$expiryDateBc."','".$strikePrice."',
			  	 																						'".$optionType."','".$openPrice."','".$highPrice."',
			  	 																						'".$lowPrice."','".$closePrice."','".$totalQtyTrade."',
			  	 																						'".$totalValueTrade ."','".$settlementPrice."','".$openInterest."')";
		    
		    if($FOLineCount > 1)
		      $resultData = mysql_query($insertData);
		    
		    $FOLineCount++;
		  }
    }
  }
		if(!$resultData)
			echo "Error........<BR>".mysql_error();
		else
			header("Location:index.php");
  fclose($handle);
  unlink("./bhavcopies/".$fileName);

  $smarty->display("storeBhavCopyMcx.tpl");

?>