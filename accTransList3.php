<?php
if(isset($comboClientId) && $comboClientId > 0)
{
  $opening = 0;
  if(isset($comboClientId))
    $currentId = $comboClientId;
  else
    $currentId = 0;
    
  $clientNameQuery="SELECT * FROM client 
                    ORDER BY firstName, middleName, lastName";
 
  $resultName=mysql_query($clientNameQuery,$link);
 
  $i=0;
  while ($resName=mysql_fetch_array($resultName))
  {
  	if($i == 0 && $currentId == 0)
      $currentId = $resName['clientId'];
      
    if(isset($comboClientId) && $resName['clientId']==$comboClientId)
    {
      $opening = $resName['opening'];
      $SELECTED[$i]="SELECTED";
    }
    else
      $SELECTED[$i]="";
    
    $resuId[$i]=$resName['clientId'];
    $resuName[$i]=$resName['firstName']." ".$resName['middleName']." ".$resName['lastName'];
    
    $i++;
  }
  $smarty->assign("resuId",$resuId);
  $smarty->assign("SELECTED",$SELECTED);
  $smarty->assign("resuName",$resuName);
  $smarty->assign("i",$i);
  
  $clientInfoQuery="SELECT * FROM cashflow 
                    WHERE clientId=".$currentId;
//  if(isset($_POST['fromDate'])
//    $clientInfoQuery = " AND 
  $clientInfoQuery.=" ORDER BY transactionDate";
  $k=0;
  $cashFlowId = array();
  $reportDate=array();
  $transType = array();
  $itemIdExpiryDate=array();
  $reportdwStatus=array();

  $reportdwAmount1=array();
  $reportdwAmount2=array();

  $reportplStatus=array();
  
  $reportplAmount1=array();
  $reportplAmount2=array();
  
  $totMargin = array();
  $totOther  = array();
  $totProfit = array();
  $totLoss   = array();
  $totWithoutMargin = array();
  
  $reportcurrentBal=array();
  $currentBal = $opening;
  
  $cashFlowId[0]       = 0;
  $reportDate[0]       = substr($_SESSION['fromDate'],8,2)."-".substr($_SESSION['fromDate'],5,2)."-".substr($_SESSION['fromDate'],2,2);
  $transType[0]        = '&nbsp;';
  $itemIdExpiryDate[0] = 'Opening';
  $reportdwStatus[0]   = 0;
  $reportdwAmount1[0]  = $opening;
  $reportdwAmount2[0]  = 0;
  $reportplStatus[0]   = 0;
  $reportplAmount1[0]  = 0;
  $reportplAmount2[0]  = 0;
  
  $storeTotMargin      = 0;
  $storeTotOther       = 0;
  $storeTotProfit      = 0;
  $storeTotLoss        = 0;
  $totMargin[0]        = $storeTotMargin;
  $totOther[0]         = $storeTotOther;
  $totProfit[0]        = $storeTotProfit;
  $totLoss[0]          = $storeTotLoss;
  $totWithoutMargin[0] = $storeTotOther+$storeTotProfit+$storeTotLoss;
  
  $reportcurrentBal[$k]=$currentBal;
  $transMode[0] = "";
  
  $clientInfoResult=mysql_query($clientInfoQuery,$link);
  while ($resInfo=mysql_fetch_array($clientInfoResult))
  {
    if($resInfo['transactionDate'] < $_SESSION['fromDate'])
    {
      /////////////////common for if and elseif :Start
      if($resInfo['dwStatus'] == 'd')
        $currentBal += $resInfo['dwAmount'];
      if($resInfo['dwStatus'] == 'w')
        $currentBal -= $resInfo['dwAmount'];
      if($resInfo['plStatus'] == 'p')
        $currentBal += $resInfo['plAmount'];
      if($resInfo['plStatus'] == 'l')
        $currentBal += $resInfo['plAmount'];
      /////////////////common for if and elseif :End
      
      if($resInfo['dwStatus'] != "d")
      {
        $reportdwAmount1[$k] += 0;
        $reportdwAmount2[$k] += $resInfo['dwAmount'];
      }
      else
      {
        $reportdwAmount1[$k] += $resInfo['dwAmount'];
        $reportdwAmount2[$k] += 0;
      }
      
      if($resInfo['plStatus'] != "p")
      {
        $reportplAmount1[$k] += 0;
        $reportplAmount2[$k] += $resInfo['plAmount'];
      }
      else
      {
        $reportplAmount1[$k] += $resInfo['plAmount'];
        $reportplAmount2[$k] += 0;
      }
      
      $reportcurrentBal[$k]=$currentBal;

      if($resInfo['transType'] == 'Margin')
      {
        if($resInfo['dwStatus'] == 'd' || $resInfo['plStatus'] == 'p')
          $storeTotMargin += $resInfo['dwAmount'];
        elseif($resInfo['dwStatus'] == 'w' || $resInfo['plStatus'] == 'l')
          $storeTotMargin -= $resInfo['dwAmount'];
      }
      else
        $storeTotOther      += $resInfo['dwAmount'];
      $totMargin[$k] = $storeTotMargin;
      
      /////////////////common for if and elseif
      $totMargin[$k] = $storeTotMargin;
      $totOther[$k]  = $storeTotOther;
      $totProfit[$k] = $storeTotProfit;
      $totLoss[$k]   = $storeTotLoss;
      $totWithoutMargin[$k] = $storeTotOther+$storeTotProfit+$storeTotLoss;
      /////////////////common for if and elseif :End
    }
    elseif($resInfo['transactionDate'] <= $_SESSION['toDate'])
    {
      /////////////////common for if and elseif :Start
      if($resInfo['dwStatus'] == 'd')
        $currentBal += $resInfo['dwAmount'];
      if($resInfo['dwStatus'] == 'w')
        $currentBal -= $resInfo['dwAmount'];
      if($resInfo['plStatus'] == 'p')
        $currentBal += $resInfo['plAmount'];
      if($resInfo['plStatus'] == 'l')
        $currentBal += $resInfo['plAmount'];
      /////////////////common for if and elseif :End
      
      $k++;
      
      $cashFlowId[$k]=$resInfo['cashFlowId'];
      $reportDate[$k]=substr($resInfo['transactionDate'],8,2)."-".substr($resInfo['transactionDate'],5,2)."-".substr($resInfo['transactionDate'],2,2);
      $transType[$k] = $resInfo['transType'];
      $itemIdExpiryDate[$k]=$resInfo['itemIdExpiryDate'];
      $reportdwStatus[$k]=$resInfo['dwStatus'];
	    $transMode[$k] = $resInfo['transMode'];
      
      if($resInfo['dwStatus'] != "d")
      {
        $reportdwAmount1[$k] = 0;
        $reportdwAmount2[$k] = $resInfo['dwAmount'];
      }
      else
      {
        $reportdwAmount1[$k] = $resInfo['dwAmount'];
        $reportdwAmount2[$k] = 0;
      }
      
      $reportplStatus[$k]=$resInfo['plStatus'];
      
      if($resInfo['plStatus'] != "p")
      {
        $reportplAmount1[$k]=0;
        $reportplAmount2[$k]=$resInfo['plAmount'];
        $storeTotLoss     += $resInfo['plAmount'];
      }
      else
      {
        $reportplAmount1[$k]=$resInfo['plAmount'];
        $reportplAmount2[$k]=0;
        $storeTotProfit      += $resInfo['plAmount'];
      }
      
      $reportcurrentBal[$k]=$currentBal;
      
      if($resInfo['transType'] == 'Margin')
      {
        if($resInfo['dwStatus'] == 'd' || $resInfo['plStatus'] == 'p')
          $storeTotMargin += $resInfo['dwAmount'];
        elseif($resInfo['dwStatus'] == 'w' || $resInfo['plStatus'] == 'l')
          $storeTotMargin -= $resInfo['dwAmount'];
      }
      else
        $storeTotOther      += $resInfo['dwAmount'];
      
      /////////////////common for if and elseif :Start
      $totMargin[$k] = $storeTotMargin;
      $totOther[$k]  = $storeTotOther;
      $totProfit[$k] = $storeTotProfit;
      $totLoss[$k]   = $storeTotLoss;
      $totWithoutMargin[$k] = $storeTotOther+$storeTotProfit+$storeTotLoss;
      /////////////////common for if and elseif :End
    }
  }
  $k++;
  $totalBalOnDate = $currentBal;
  
  $smarty->assign("fromDate", substr($_SESSION['fromDate'],8,2)."-".substr($_SESSION['fromDate'],5,2)."-".substr($_SESSION['fromDate'],2,2));
  $smarty->assign("toDate",   substr($_SESSION['toDate'],8,2)."-".substr($_SESSION['toDate'],5,2)."-".substr($_SESSION['toDate'],2,2));
  
  $smarty->assign("transMode",$transMode);
  $smarty->assign("opening",$opening);
  $smarty->assign("cashFlowId",$cashFlowId);
  $smarty->assign("reportDate",$reportDate);
  $smarty->assign("transType",$transType);
  $smarty->assign("itemIdExpiryDate",$itemIdExpiryDate);
  $smarty->assign("reportdwStatus",$reportdwStatus);

  $smarty->assign("reportdwAmount1",$reportdwAmount1);
  $smarty->assign("reportdwAmount2",$reportdwAmount2);

  $smarty->assign("reportplStatus",$reportplStatus);

  $smarty->assign("reportplAmount1",$reportplAmount1);
  $smarty->assign("reportplAmount2",$reportplAmount2);

  $smarty->assign("totMargin",       $totMargin);
  $smarty->assign("totOther" ,       $totOther);
  $smarty->assign("totProfit",       $totProfit);
  $smarty->assign("totLoss",         $totLoss);
  $smarty->assign("totWithoutMargin", $totWithoutMargin);
  
  $smarty->assign("reportcurrentBal",$reportcurrentBal);
  $smarty->assign("storeTotMargin",$storeTotMargin);
  $smarty->assign("totalBalOnDate",$totalBalOnDate);
  $smarty->assign("k",$k);
  $smarty->display("accTransList3.tpl");
}
?>