<?php
session_start();
if(!isset($_SESSION['toDate']))
{
  header("Location: selectDtSession.php?goTo=cashFlow");
}
else
{
  include "./etc/om_config.inc";
  $smarty=new smartyWWW();
  $opening       = 0;
  $selectBankId  = 0;
  $totalAmount   = 0;
  $cashFlowCount = 0;
  $otherExpCount = 0;
  $cashFlow      = array();
  $otherExp      = array();
  
  $bankQuery = "SELECT * FROM bankmaster
                 ORDER BY bankName";
  $bankResult = mysql_query($bankQuery);
  $b = 0;
  while($bankRow = mysql_fetch_array($bankResult))
  {
    $bank['id'][$b]   = $bankRow['bankId'];
    $bank['name'][$b] = $bankRow['bankName'];
    $b++;
  }
  if(isset($_POST['transModeOpt']) && $_POST['transModeOpt'] > 0 )
    $selectBankId = $_POST['transModeOpt'];
    
  $selectQuery = "SELECT * FROM cashflow";
  if($selectBankId > 0)
    $selectQuery .= " WHERE transMode = (SELECT bankName FROM bankmaster WHERE bankId = ".$selectBankId.")"; 
  
  $selectQuery .= " ORDER BY transactionDate";
  $selectQueryResult = mysql_query($selectQuery);
  $k = 0;
  while($rowFound = mysql_fetch_array($selectQueryResult))
  {
    $cashFlow[$k]['mode']      = $rowFound['transMode'];
    $cashFlow[$k]['id']        = $rowFound['cashFlowId'];
    $cashFlow[$k]['date']      = $rowFound['transactionDate'];
    $cashFlow[$k]['transtype'] = $rowFound['transType'];
    $cashFlow[$k]['notes']     = $rowFound['itemIdExpiryDate'];
    $cashFlow[$k]['dwstatus']  = $rowFound['dwStatus'];
    $cashFlow[$k]['amount']    = $rowFound['dwAmount'];
    if($rowFound['dwStatus'] == "d")
    {
      $totalAmount  += $rowFound['dwAmount'];
    }
    elseif($rowFound['dwStatus'] == "w")
    {
      $totalAmount  -= $rowFound['dwAmount'];
    }
    $cashFlow[$k]['totalAmount']  = $totalAmount;
    $k++;
  }
  $cashFlowCount = count($cashFlow);
  
  // This For Get The Data From Other Exp : Start
  $selectExp = "SELECT * FROM otherexp";
  if($selectBankId > 0)
    $selectExp .=" WHERE otherExpMode = (SELECT bankName FROM bankmaster WHERE bankId = ".$selectBankId.")";
  
  $selectExp .= " ORDER BY otherExpDate";
  $selectExpResult = mysql_query($selectExp);
  $i = 0;
  while($expRowFound = mysql_fetch_array($selectExpResult))
  {
    $otherExp[$i]['id']          = $expRowFound['otherexpId'];
    $otherExp[$i]['amount']      = $expRowFound['otherExpAmount'];
    $otherExp[$i]['date']        = $expRowFound['otherExpDate'];
    $otherExp[$i]['name']        = $expRowFound['otherExpName'];
    $otherExp[$i]['note']        = $expRowFound['note'];
    $totalAmount                -= $expRowFound['otherExpAmount'];
    $otherExp[$i]['totalAmount'] = $totalAmount;
    $i++;
  }
  $otherExpCount = count($otherExp);
  // This For Get The Data From Other Exp : End
  
  $smarty->assign("bank",$bank);
  $smarty->assign("selectBankId",$selectBankId);
  $smarty->assign("cashFlow",$cashFlow);
  $smarty->assign("cashFlowCount",$cashFlowCount);
  $smarty->assign("otherExp",$otherExp);
  $smarty->assign("otherExpCount",$otherExpCount);

  $smarty->display("cashFlow.tpl");
}
?>
