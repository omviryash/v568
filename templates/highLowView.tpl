<HTML>
<HEAD><TITLE>Add Client</TITLE></HEAD>
<BODY bgColor="#FFCEE7">
<FORM name="form1" action="{$PHP_SELF}" METHOD=POST>
<A href="./index.php">Home</A>
<center><font style="font-size: 16px"><b>! Trades List !</b></font></center>
<table border='1' cellpadding='1' cellspacing='2' align='center'>
<TR>
  <TD align="center" colspan="13">
    Date :
    {html_select_date time=$itemDate prefix="itemRateDate" start_year="-2" end_year="+1" month_format="%m"  field_order="DMY" day_value_format="%02d"}   
    <INPUT type="submit" name="dateBtn" value="Go!">   
  </TD>
</TR>  
<tr>
  <th align='center' >No</th>
  <th align='center' >Standing</th>
	<th align='center' >Client</th>
	<th align='center' >BuySell</th>
	<th align='center' >Item</th>
	<th align='center' >Trade Date</th>
	<th align='center' >qty</th>
	<th align='center' >Price</th>
	<th align='center' >ExpiryDate</th>
	<th align='center' >Confirmed</th>
	<th align='center' >Exchange</th>
</tr>
<tbody>
{section name="sec" loop=$tradeArray}
<tr>
  <td align="right">{$tradeArray[sec].tradeId}</td>
  <td align="right">{$tradeArray[sec].standing}</td>
  <td align="left">{$tradeArray[sec].firstName}</td>
  <td align="left">{$tradeArray[sec].buySell}</td>
  <td align="left">{$tradeArray[sec].itemName}</td>
  <td align="left" NOWRAP>{$tradeArray[sec].tradeDate}</td>
  <td align="right">{$tradeArray[sec].qty}</td>
  <td align="right">{$tradeArray[sec].price}</td>
  <td align="left">{$tradeArray[sec].expiryDate}</td>
  <td align="right">{$tradeArray[sec].confirmed}</td>
  <td align="left">{$tradeArray[sec].exchange}</td>
</tr>
{sectionelse}
<tr>
  <th colspan="13">
    <font color="red">"Record Not Found !"</font>
  </th>
</tr>
{/section}
</table>
</form>
</BODY>
</HTML>