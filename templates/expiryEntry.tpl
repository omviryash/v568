<html>
<HEAD>
<TITLE>Expiry Date Entry Form</TITLE>

<SCRIPT language="javascript">
  /* for exchange change combo :Start */
  {literal}
  function companyChange(theObject)
  {
    var form = theObject.form;
    for(i=0;i<form.elements.length;i++)
    {
      if(form.elements[i]==theObject)
      {
        exchange = document.form1.exchange;
        itemId = document.form1.itemId;
      }
    }
    itemId.options.length = 0;
  {/literal}
  {section name="sec1" loop=$exchange}
    if(exchange.selectedIndex == {%sec1.index%} )
  {literal}
    {
  {/literal}
      {section name="sec2" loop=$itemId[sec1]}
        itemId.options[{%sec2.index%}]=new Option("{$itemId[sec1][sec2]}","{$itemId[sec1][sec2]}");
      {/section}
  {literal}
    }
  {/literal}
  {/section}
  {literal}
  }
  {/literal}
/* for exchange change combo :End */
  </SCRIPT>
</HEAD>
<BODY bgColor="#FFCEE7">
<FORM name=form1 action="{$PHP_SELF}" method = POST>
<A href="./index.php">Home</A>
<A href="expiryList.php">List</A>
<BR><BR>
<B>Set Expiry Date : </B>
<TABLE border="1">
  
<TR>
  <TD>Exchange</TD>
  <TD>
    <SELECT name="exchange" onChange="companyChange(this);">
  {html_options values="$exchange" output="$exchange"}
</SELECT></TD></TR>
<TR>
  <TD>Item Name</TD>
<TD><SELECT name="itemId">
  <option value='0'>Select Id</option>
</SELECT>
</TR>
<TR>
  <TD>Expiry Date</TD>
   <TD> {html_select_date prefix="expiryDate" start_year=1990 end_year=2025 day_value_format="%02d" month_value_format="%b" month_format="%b" field_order="DMY"}</TD>
</TR>
</TABLE>
<BR>
<INPUT type = submit Value=SAVE>
</FORM>

<SCRIPT language="javascript">
  document.form1.elements[0].focus();
  companyChange(document.form1.exchange);
</SCRIPT>
</BODY>
</HTML>