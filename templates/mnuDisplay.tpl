<TABLE cellPadding="8" cellspacing="0" border="1">
<TR>
  <TD align="right"><B>Master : </B></TD>
  <TD><A href='brokerage.php'>Items</A></TD>
  <TD><A href='expiryList.php'>Expiry Dates</A></TD>
  <TD><A href='clientList.php'>Clients</A>&nbsp;&nbsp;<A href='clientList2.php'>New</A></TD>
  <TD><A href="vendorList.php">Vendors</A></TD>
  <TD><A href="exchangeAdd.php">Add Exachange</A></TD>
  <TD><A href="changePassword.php">Change Password</A></TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B>Auto Store : </B></TD>
  <TD><A href="txtFile.php">TWS</A></TD>
  <TD><A href="txtFileOdin.php">Odin</A></TD>
  <TD><A href="takeMCXTrades.php">Full Auto TWS</A></TD>
  <TD><A href="takeODINTrades.php">Full Auto ODIN</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B>Store : </B></TD>
  <TD><A href='' onClick="tradeWindow=window.open('tradeAdd.php', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=130, left=1, top=300'); return false;">Add Trade</A></TD>
  <TD><A href='' onClick="tradeWindow=window.open('tradeAdd3.php', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=130, left=1, top=300'); return false;">Add Trade</A></TD>
  <TD><A href='' onClick="tradeWindow=window.open('tradeAddNew.php', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=160, left=1, top=300'); return false;">Add Trade 2 Entry</A></TD>
  <TD><A href='' onClick="tradeWindow=window.open('tradeAddReverse.php', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=165, left=1, top=300'); return false;">Add Trade Reverse</A>
      <A href='' onClick="tradeWindow=window.open('tradeAddReverseCopy.php', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=165, left=1, top=300'); return false;">New</A>
  </TD>
  <TD>
    <A href='' onClick="tradeWindow=window.open('tradeAdd7.php', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=165, left=1, top=300'); return false;">Trade 7</A>
  </TD>
  <TD><A href='' onClick="tradeWindow=window.open('tradeAdd8.php', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=130, left=1, top=300'); return false;">Add Trade 8</A></TD>
  <TD><A href='' onClick="tradeWindow=window.open('tradeAdd9.php', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=165, left=1, top=300'); return false;">Add Trade 9</A>&nbsp;&nbsp;
      <A href='' onClick="tradeWindow=window.open('tradeAdd9Ajax.php', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=165, left=1, top=300'); return false;">Ajax</A></TD>
</TR>
<TR>
  <TD align="right"><B>Trades : </B></TD>
  <TD>
    <A href='clientTrades.php'>Trades</A>&nbsp;&nbsp;
  </TD>
  <TD><A href='clientTradesLot2Side.php'>Trades 2Side</A>&nbsp;&nbsp;</TD>
  <TD><A href='clientTradesPer1side.php'>Trades %1Side</A>&nbsp;&nbsp;</TD>
  <TD><A href='clientTradesPer2side2.php'>Trades %2Side</A>&nbsp;&nbsp;</TD>
  <TD><A href='clientTradesExchange.php'>Trades with Exchange</A></TD>
  <TD><A href='clientTrades.php?display=detailed'>Detailed Trades</A></TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B>Print : </B></TD>
  <TD>
    <A href='clientTrades.php?display=tradesPrint'>Print</A> &nbsp;&nbsp; 
    <A href='clientTrades.php?display=tradesPrint2'>Print2</A>
  </TD>
  <TD>
    <A href='clientTradesLot2Side.php?display=tradesPrint'>Print</A> &nbsp;&nbsp; 
    <A href='clientTradesLot2Side.php?display=tradesPrint2'>Print2</A>
  </TD>
  <TD>
    <A href='clientTradesPer1side.php?display=tradesPrint'>Print</A> &nbsp;&nbsp; 
    <A href='clientTradesPer1side.php?display=tradesPrint2'>Print2</A>
  </TD>
  <TD>
    <A href='clientTradesPer2side2.php?display=tradesPrint'>Print</A> &nbsp;&nbsp;
    <A href='clientTradesPer2side2.php?display=tradesPrint2'>Print2</A>
  </TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B>Gross : </B></TD>
  <TD><A href='clientTrades.php?display=gross'>Gross</A></TD>
  <TD><A href='clientTradesLot2Side.php?display=gross'>Gross</A></TD>
  <TD><A href='clientTradesPer1side.php?display=gross'>Gross</A></TD>
  <TD><A href='clientTradesPer2side2.php?display=gross'>Gross</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B> : </B></TD>
  <TD><A href='clientTrades.php?display=itemPending'>Item Pending</A></TD>
  <TD><A href='clientTrades.php?display=itemPending2'>Item Pending2</A></TD>
  <TD><A href='clientTrades.php?display=itemWiseGross'>Item Gross</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B> : </B></TD>
  <TD><A href="selectDtSession.php">Date range</A></TD>
  <TD><A href="tradeListForCrossChecking.php">Confirm</A></TD>
  <TD><A href="deleteAll.php">DeleteAll</A></TD>
  <TD>&nbsp;</TD>
  <TD><A href="attachNameToTxtData.php">PendingTrade</A></TD>
  <TD><A href='attachNameToTxtDataAll.php'>Change Name</A></TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B> : </B></TD>
  <TD><A href="orderList.php">Order List</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B> : </B></TD>
  <TD><A href="mnuStand.php">Standing</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B> : </B></TD>
  <TD><A href='mnuAccount.php'>Money Transactions</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B>Vendor : </B></TD>
  <TD><A href="vendorTrades.php">1 Side</A></TD>
  <TD><A href="vendorTradesHalfSide.php">1/2 Both Side</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B> : </B></TD>
  <TD><A href="settings.php">Settings</A></TD>
  <TD><A href="writeTradesFile.php">Online Backup</A></TD>
  <TD><A href="http://localhost/pma/db_details_export.php?lang=en-utf-8&server=1&collation_connection=utf8_general_ci&db=ommcx&goto=db_details_export.php&selectall=1">Backup</A></TD>
  <TD>&nbsp;</TD>
  <TD><A href='goldSilverCalcJs.php'>Calculator1</A></TD>
  <TD><A href='calc.php'>Calculator2</A></TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B> : </B></TD>
  <TD><A href='clientTrades.php?display=noInnerStand'>No Inner Stand</A></TD>
  <TD><A href='clientTrades.php?display=openStand'>Open Stand</A></TD>
  <TD><A href='clientTrades.php?display=closeStand'>Close Stand</A></TD>
  <TD><A href='clientTrades.php?display=openColoseStand'>Open & Close</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD align="right"><B> : </B></TD>
  <TD><A href="links.php">Useful Links</A></TD>
  <TD><A href="extraMenu.php">Extra Menu</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
</TABLE>






<TABLE cellPadding="8" cellspacing="0" border="1">
<TR>
  <TD colspan="7" align="center"><B>Broker</B></TD>
</TR>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>
    <TABLE border="0" cellspacing="0" cellPadding="0" width="100%">
    <TR><TD><A href='brokerTrades.php'>Trades</A>&nbsp;&nbsp;</TD>
<!--    <A href='clientTrades2Side.php'>2</A>
    &nbsp;&nbsp;-->
        <TD align="right"><A href='brokerTrades.php?display=tradesPrint'>Print</A> &nbsp;&nbsp; <A href='brokerTrades.php?display=gross'>Gross</A></TD></TR>
    </TABLE>
  </TD>
  <TD><A href='brokerTrades.php?display=detailed'>Detailed Trades</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>
    <TABLE border="0" cellspacing="0" cellPadding="0" width="100%">
    <TR><TD><A href='brokerTradesLot2Side.php'>Trades 2Side</A>&nbsp;&nbsp;</TD>
        <TD align="right"><A href='brokerTradesLot2Side.php?display=tradesPrint'>Print</A> &nbsp;&nbsp; <A href='brokerTradesLot2Side.php?display=gross'>Gross</A></TD></TR>
    </TABLE>
  </TD>
  <TD><A href='brokerTrades.php?display=noInnerStand'>No Inner Stand</A></TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>
    <TABLE border="0" cellspacing="0" cellPadding="0" width="100%">
    <TR><TD><A href='brokerTradesPer1side.php'>Trades %1Side</A>&nbsp;&nbsp;</TD>
        <TD align="right"><A href='brokerTradesPer1side.php?display=tradesPrint'>Print</A> &nbsp;&nbsp; <A href='brokerTradesPer1side.php?display=gross'>Gross</A></TD></TR>
    </TABLE>
  </TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
<TR>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>
    <TABLE border="0" cellspacing="0" cellPadding="0" width="100%">
    <TR><TD><A href='brokerTradesPer2side2.php'>Trades %2Side</A>&nbsp;&nbsp;</TD>
        <TD align="right"><A href='brokerTradesPer2side2.php?display=tradesPrint'>Print</A> &nbsp;&nbsp;<A href='brokerTradesPer2side2.php?display=gross'>Gross</A></TD></TR>
    </TABLE>
  </TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
  <TD>&nbsp;</TD>
</TR>
</TABLE>
