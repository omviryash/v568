<FORM name="dateForm" action="dateRange.php" method="post">
<TABLE border="0" cellPadding="2" cellSpacing="0">
<TR>
  <TD>From : </TD>
  <TD>{html_select_date time="$fromDate" prefix="fromDate" start_year="-1" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
  </TD>
  <TD>To : </TD>
  <TD>
    {html_select_date time="$today" prefix="toDate" start_year="-1" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
  </TD>
</TR>
</TABLE>
</FORM>