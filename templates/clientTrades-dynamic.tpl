<HTML>
<HEAD>
<TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{
literal
}
td {
	font-weight: BOLD
}
.lossStyle {
	color: red
}
.profitStyle {
	color: blue
}
</STYLE>
<script src="./js/jquery.min.js"></script>
<script type="text/javascript">
function confirmedCheckbox(myObj, objValues)
{
  if($(myObj).is(':checked') == 1)
    $(".confirmed"+objValues).attr('checked', true);
  else
    $(".confirmed"+objValues).attr('checked', false);
}  
/*function changeExchange()
{
  if($("#exchange").val() == "MCX")
    window.location.href = "./clientTrades.php";
  else if($("#exchange").val() == "Comex")
    window.location.href = "./clientTradesComex.php";
}*/

jQuery(document).ready(function($){

  $("#storeLocally").click(function() {
    // alert("Hello");
    var calculations = [];
    $(".price").each(function(i, ele) {
      calculations.push({
        'item': $(ele).data('item'),
        'expiry': $(ele).data('expiry'),
        'value': $(ele).val()
      });
    }).promise().done(function() {
      window.sessionStorage.setItem('calculations', JSON.stringify(calculations));
    });
  });


  $(".price").change(function(){
    var self = this;
    var item = $(self).data('item');
    var expiry = $(self).data('expiry');
    var list = $(".data-holder[data-item="+item+"][data-expiry="+expiry+"]");
    var clients = {};
    $(list).each(function(i, row) {
      var qty = $(row).data("qty");
      var price = $(row).data('price');
      var client = $(row).data("client");
      var transection = $(row).data('transection-type');

      qty = transection == "Sell" ? qty * (-1): qty;

      if(clients.hasOwnProperty(client)) {
        clients[client].landingCost += (qty * price);
        clients[client].totalQty += qty;
      } else {
        clients[client] = {
          'landingCost': (qty * price),
          'totalQty': qty
        }
      }
    }).promise().done(function(){
      var item = $(self).data('item');
      var expiry = $(self).data('expiry');
      var totalAmount = 0;
      for(clientId in clients) {
        var clientDetails = clients[clientId];
        var calculatedAmount = (clientDetails.totalQty * $(self).val()) - clientDetails.landingCost ;
        totalAmount += calculatedAmount;
        $(".calculation-holder[data-client="+clientId+"][data-item="+item+"][data-expiry="+expiry+"]")
          .text(calculatedAmount)
          .removeClass("lossStyle")
          .removeClass("profitStyle")
          .addClass((calculatedAmount < 0 ?"lossStyle": "profitStyle"));
      }
      $(".total-amount-holder[data-item="+item+"][data-expiry="+expiry+"]")
        .text(totalAmount)
        .removeClass("lossStyle")
        .removeClass("profitStyle")
        .addClass((totalAmount < 0 ?"lossStyle": "profitStyle"));
      // total profit loss count
      var grandTotal = 0;
      $(".total-amount-holder").each(function(i, amtEle) {
        grandTotal += parseInt($(amtEle).text());
      }).promise().done(function() {
        $("#wholepl1")
        .text(grandTotal)
        .removeClass("lossStyle")
        .removeClass("profitStyle")
        .addClass((grandTotal < 0 ?"lossStyle": "profitStyle"));
      });
    });
    // OLD CODE COMMENTED BY HK
    // var rowids = $(this).closest('tr').attr('id');     
    // var expival = $("#"+rowids+"_exp").val();
    // var itemval = $("#"+rowids+"_item").val();
    // var totprice = 0;
    // $("#itemtable tr").each(function(){
    //  var rowid = $(this).attr('id'); 
    //  var rowcls = $(this).attr('class');       
    //  var expval = $("#"+rowid+"_exp").val();
    //  var ival = $("#"+rowid+"_item").val();
    //  if(expval == expival && itemval == ival){
    //    var buy_qty = $("#"+rowid+"_buy").val();
    //    var sell_qty = $("#"+rowid+"_sell").val();
    //    //var quanval = sell_qty-buy_qty;
    //    //$("#row_"+rowcls+"_quan").html(quanval);
    //    var buyprice = 0;               
    //    var clientIds = $("#"+rowid+"_client").val();
    //    $("."+clientIds+"_"+ival+"_"+expival+"_buyprice").each(function(){
    //      buyprice = parseInt(buyprice)+parseInt($(this).val());              
    //    });
    //    //var quanval = $("#row_"+rowcls+"_quan").html();     
    //    var priceval = $("#"+rowids+"_price").val();
    //    var price = (sell_qty*priceval)-buyprice;
    //    totprice = totprice+price;
        
    
    //    /*if(quanval < 0){
    //      $("#row_"+rowcls+"_quan1").html(quanval.replace('-', '')+" qty, Each one sell at "+priceval);
    //    } else {
    //      $("#row_"+rowcls+"_quan1").html(quanval+" qty buy at "+priceval);
    //    }*/
        
    //    $("#row_"+rowcls+"_price").html(price);
    //    $("#whole_"+ival+"_"+expival).html(totprice);
    //    $("#whole1_"+ival+"_"+expival).html(totprice);
    //    $("#wholeval_"+ival+"_"+expival).val(totprice);           
    //    var clientTotal = 0;
    //    $("."+clientIds+"_pl").each(function(){
        
    //    if($(this).html()!=''){
    //      clientTotal = parseInt(clientTotal) + parseInt($(this).html());
    //      $("#"+clientIds+"_itemtotal").html(clientTotal);
    //      }
    //    });
    //  }
                
    // });
    // var totalprice = 0;
    //    $(".wholeprice").each(function(){
    //      var wholeval = $(this).val();
    //      if(wholeval!=''){
    //        totalprice = parseInt(totalprice)+parseInt(wholeval); 
    //      }
    //    });
    // $("#wholepl").html(totalprice);
    // $("#wholepl1").html(totalprice);
  });

  var calculations = eval("("+window.sessionStorage.getItem('calculations')+")");

  if(calculations != undefined) {
    for(i in calculations) {
      var ele = calculations[i];
      var htmlEle = $(".price[data-item="+ele.item+"][data-expiry="+ele.expiry+"]");
      $(htmlEle).val(ele.value);
      $(htmlEle).change();
    }
  }

});
{/literal}
</script>
</HEAD>
<BODY bgColor="#FFFF80">
<FORM name="form1" method="get" action="{$PHP_SELF}">
  <INPUT type="hidden" name="display" value="{$display}">
  <INPUT type="hidden" name="itemIdChanged" value="0">
  <input type="hidden" name="ex" value="{$ex}">  
  <table cellPadding="5" cellSpacing="5" border="1" style="margin-bottom:20px;">
    <tr>
      <th>Item</th>
      <th>Expiry</th>
      <th>Current Price</th>
    </tr>
    {foreach from=$expiryDetails item=eDetails}
    <tr id="val_{$eDetails.id}">
      <td>{$eDetails.itemId}
        <input type="hidden" id="val_{$eDetails.id}_item" value="{$eDetails.itemId}"></td>
      <td>{$eDetails.expiryDate}
        <input type="hidden" id="val_{$eDetails.id}_exp" value="{$eDetails.expiryDate}"></td>
      <td><input
    type="text"
    class="price"
    id="val_{$eDetails.id}_price"
    data-expiry="{$eDetails.expiryDate}"
    data-item="{$eDetails.itemId}"
  ></td>
    </tr>
    {/foreach}
    <tr>
      <td colspan="3" style="text-align: right"><button id="storeLocally">Save</button></td>
    </tr>
  </table>
  <TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
    <TR>
      <TD>Client :
        <SELECT name="clientId" onChange="document.form1.submit();">
          
    {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOptions"}
    
        </SELECT></TD>
      <TD>Item :
        <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
          
    {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}
    
        </SELECT></TD>
      <TD> Buy Sell :
        <select name="buySellOnly" onChange="document.form1.submit();">
          <option value="All"  {$buySellOnlyAll}>All</option>
          <option value="Buy"  {$buySellOnlyBuy}>Buy</option>
          <option value="Sell" {$buySellOnlySell}>Sell</option>
        </select></TD>
      <TD>Group :
        <select name="groupName" onChange="document.form1.submit();">
          
    {html_options values=$groupName output=$groupName selected=$groupIdSelected}
  
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Expiry :
        <SELECT name="expiryDate" onChange="document.form1.submit();">
          
    {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOptions"}
    
        </SELECT>
        <!--Exchange :
        <SELECT name="exchange" id="exchange" onChange="changeExchange();">
          <option value="MCX" {$exchangeOnlyMCX}>MCX</option>
          <option value="Comex" {$exchangeOnlyComex}>Comex</option>
        </SELECT>--></TD>
    </TR>
    <TR>
      <TD colspan="3" align="center"><A href="selectDtSession.php?goTo=clientTrades">Date range</A> : {$fromDate} To : {$toDate}
        </CENTER></TD>
    </TR>
    <TR>
      <TD colspan="3" align="center"> {$message} </TD>
    </TR>
  </TABLE>
</FORM>
<FORM name="form2" method="post" action="{$PHP_SELF}">
  <INPUT type="hidden" name="display" value="{$display}">
  <INPUT type="hidden" name="itemIdChanged" value="0">
  <input type="hidden" name="clientId" value="{$clientIdSelected}">
  <input type="hidden" name="itemId" value="{$itemIdSelected}">
  <input type="hidden" name="buySellOnly" value="{$buySellOnlySelected}">
  <input type="hidden" name="groupName" value="{$groupIdSelected}">
  <input type="hidden" name="expiryDate" value="{$expiryDateSelected}">
  <input type="hidden" name="exchange" value="{$exchangeSelected}">
  <TABLE border="1" cellPadding="2" cellSpacing="0" id="itemtable">
  <!-- Table header 1st row -->
  <TR>
    <TD>&nbsp;</TD>
    <TD colspan="2" align="center">Buy</TD>
    <TD>&nbsp;</TD>
    <TD colspan="2" align="center">Sell</TD>
    <TD colspan="6">&nbsp;</TD>
  </TR>
  <!-- Table header 2nd row -->
  <TR>
    <TD align="center">BuySell</TD>
    <TD align="center">Qty</TD>
    <TD align="center">Price</TD>
    <TD>&nbsp;</TD>
    <TD align="center">Qty</TD>
    <TD align="center">Price</TD>
    <TD align="center">Date</TD>
    <TD align="center">Stand</TD>
    <TD align="center">Item</TD>
    <TD align="center">NetProfitLoss</TD>
    <TD align="center">Vendor</TD>
    <TD align="center">Delete</TD>
    {if $display == "detailed"}
    <TD align="center">UserRemarks</TD>
    <TD align="center">OwnClient</TD>
    <TD align="center" NOWRAP>TradeRefNo</TD>
    {/if} </TR>
  <!-- Table blank row after header -->
  <TR>
    <TD colspan="2">&nbsp;</TD>
    <TD colspan="3" align="center"></td>
    <TD colspan="7">&nbsp;</td>
  </TR>
  <!-- Loop --> 
  {section name="sec1" loop="$trades"}
  {if $trades[sec1].clientId != $trades[sec1].prevClientId or $trades[sec1].itemId != $trades[sec1].prevItemId or $trades[sec1].expiryDate != $trades[sec1].prevExpiryDate}
  <TR >
    <TD colspan="2"><U>{$trades[sec1].clientId} : {$trades[sec1].clientName}</U>&nbsp;:&nbsp;({$trades[sec1].clientDeposit})</TD>
    <TD colspan="3" align="center"><input type="submit" name="confirmBtn" value="! Done !" />
      <input type="checkbox" name="checkAll" class="checkAll{$trades[sec1].clientId}{$trades[sec1].itemIdExpiry}" onClick="confirmedCheckbox(this,'{$trades[sec1].clientId}{$trades[sec1].itemIdExpiry}')"></td>
    <TD colspan="3">&nbsp;</td>
    <TD colspan="4" align="left">{$trades[sec1].itemIdExpiry} </TD>
  </TR>
  {/if}
  <TR style="color:{$trades[sec1].fontColor}"
    class="data-holder"
    data-client="{$trades[sec1].clientId}"
    data-item="{$trades[sec1].itemId}"
    data-expiry="{$trades[sec1].expiryDate}"
    data-transection-type="{$trades[sec1].buySell}"
    data-qty="{if ($trades[sec1].buySell == 'Buy')}{$trades[sec1].buyQty}{else}{$trades[sec1].sellQty}{/if}"
    data-price="{if ($trades[sec1].buySell == 'Buy') }{$trades[sec1].price}{else}{$trades[sec1].sellPrice}{/if}"

>
    <TD align="center">{$trades[sec1].buySell}</TD>
    <TD align="right">{$trades[sec1].buyQty}</TD>
    <TD align="right">{$trades[sec1].price}</TD>
    <TD align="right"><input type="checkbox" name="confirmed[{$trades[sec1].tradeId}]" class="confirmed{$trades[sec1].clientId}{$trades[sec1].itemIdExpiry}" 
           value="{$trades[sec1].tradeId}" {if $trades[sec1].confirmed == 1}CHECKED{/if}></TD>
    <TD align="right">{$trades[sec1].sellQty}</TD>
    <TD align="right">{$trades[sec1].sellPrice}
      <input type="hidden" class="{$trades[sec1].clientId}_{$trades[sec1].itemId}_{$trades[sec1].expiryDate}_buyprice" value={$trades[sec1].buyQty*$trades[sec1].price}></TD>
    <TD align="center" NOWRAP> {$trades[sec1].tradeDate}
      {if $display == "detailed"} {$trades[sec1].tradeTime} {/if} </TD>
    <TD align="center"> {if $trades[sec1].standing != "Open" && $trades[sec1].standing != "Close"}
      {if $trades[sec1].highLowConf == 1} <font style="bgColor: black;color:{if $trades[sec1].fontColor == "red"}black{else}black{/if};"><b>@@@</b></font> {/if}
      {/if}
      {$trades[sec1].standing} </TD>
    <TD align="center" NOWRAP>{$trades[sec1].itemIdExpiry}</TD>
    <TD>&nbsp;</TD>
    <TD align="center">{$trades[sec1].vendor}&nbsp;</TD>
    <TD><A onClick="return confirm('Are you sure?');" href="deleteTxt.php?goTo={$goTo}&tradeId={$trades[sec1].tradeId}"> {if $trades[sec1].standing != "Close"}
      Delete
      {else}
      Delete Stand
      {/if} </A> &nbsp;&nbsp; <A href="tradeAdd3Edit.php?tradeId={$trades[sec1].tradeId}">Edit</a></TD>
    {if $display == "detailed"}
    <TD align="center">{$trades[sec1].userRemarks}</TD>
    <TD align="center">{$trades[sec1].ownClient}</TD>
    <TD align="center" NOWRAP>{$trades[sec1].tradeRefNo}</TD>
    {/if} </TR>
  {if $trades[sec1].dispGross != 0}
  <TR id="row_{$trades[sec1].tradeId}" class="{$trades[sec1].clientId}_{$trades[sec1].tradeId}">
    <TD align="right" NOWRAP> Net: {math equation="totBuyQty-totSellQty" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty} </TD>
    <TD align="right">{$trades[sec1].totBuyQty}
      <input type="hidden" value="{$trades[sec1].totBuyQty}" id="row_{$trades[sec1].tradeId}_buy"></TD>
    <TD align="right">{$trades[sec1].buyRash}</TD>
    <TD>&nbsp;</TD>
    <TD align="right">{$trades[sec1].totSellQty}
      <input type="hidden" value="{$trades[sec1].totBuyQty}" id="row_{$trades[sec1].tradeId}_sell"></TD>
    <TD align="right">{$trades[sec1].sellRash}</TD>
    <input type="hidden" id="row_{$trades[sec1].tradeId}_exp" value="{$trades[sec1].expiryDate}">
    <input type="hidden" id="row_{$trades[sec1].tradeId}_item" value="{$trades[sec1].itemId}">
    <input type="hidden" id="row_{$trades[sec1].tradeId}_client" value="{$trades[sec1].clientId}">
    <td id="row_{$trades[sec1].tradeId}_quan"></td>
    <td id="row_{$trades[sec1].tradeId}_price"></td>
    <td id="row_{$trades[sec1].tradeId}_quan1"></td>
    {if $trades[sec1].totBuyQty == $trades[sec1].totSellQty}
    <TD colspan="3" align="right" NOWRAP> {if $trades[sec1].profitLoss < 0} <FONT class="lossStyle">Loss : 
      {else} <FONT class="profitStyle">Profit : 
      {/if}
      {$trades[sec1].profitLoss}</FONT> <!--Brok       : {$trades[sec1].oneSideBrok} <br />
      {$trades[sec1].brok1} <br />
      {$trades[sec1].brok2}--> </TD>
    <!--<TD align="right" NOWRAP> {if $trades[sec1].netProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
      {$trades[sec1].netProfitLoss}</FONT></TD>
    <TD colspan="2">&nbsp;</TD>-->
    {else}
    <TD colspan="6">{$trades[sec1].itemIdExpiry} : Buy Sell Qty Not Same</TD>
    {/if}
  </TR>  
  <TR id="{$trades[sec1].clientId}_{$trades[sec1].tradeId}" > 
    <!--td colspan="3">Quantity: <span id="row_{$trades[sec1].clientId}_{$trades[sec1].tradeId}_quan"></span></td-->
    <td colspan="4">Profit/Loss: <span 
      id="row_{$trades[sec1].clientId}_{$trades[sec1].tradeId}_price"
      class="{$trades[sec1].clientId}_pl calculation-holder"
      data-client="{$trades[sec1].clientId}"
      data-item="{$trades[sec1].itemId}"
      data-expiry="{$trades[sec1].expiryDate}"> </span></td>
    <!--td colspan="5">Quan With Price: <span id="row_{$trades[sec1].clientId}_{$trades[sec1].tradeId}_quan1"></span></td--> 
  </TR>
  </TR>
  {if $trades[sec1].dispClientWhole != 0}
  <TR>
    <TD colspan="6" align="right"> : Total : {$trades[sec1].clientId} : {$trades[sec1].clientName} </TD>
    <TD colspan="3" align="right"><U> {if $trades[sec1].clientTotProfitLoss < 0} <FONT class="lossStyle">Loss : 
      {else} <FONT class="profitStyle">Profit : 
      {/if} <span id="{$trades[sec1].clientId}_itemtotal">{$trades[sec1].clientTotProfitLoss}</span></FONT></U> <!--Brok       : {$trades[sec1].clientTotBrok} <br />
      {$trades[sec1].clientTotBrok1} <br />
      {$trades[sec1].clientTotBrok2}--></TD>
    <TD align="right"><!--<U> {if $trades[sec1].clientTotNetProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
      {$trades[sec1].clientTotNetProfitLoss}</FONT></U>--></TD>
    <TD align="center" colspan="2">&nbsp;</TD>
  </TR>
  {/if}
  {/if}
  {/section}
  <TR>
    <TD colspan="2">&nbsp;</TD>
    <TD colspan="3" align="center"><input type="submit" name="confirmBtn" value="! Done !" /></td>
    <TD colspan="7">&nbsp;</td>
  </TR>
  <TR>
    <TD align="center">Net</TD>
    <TD>Buy</TD>
    <TD>Rash</TD>
    <TD>&nbsp;</TD>
    <TD>Sell</TD>
    <TD>Rash</TD>
    <TD colspan="2" align="center">Item</TD>
    <TD align="center">ProfitLoss</TD>
    <!--<TD colspan="2" align="center">Brok</TD>
    <TD align="center">NetProfitLoss</TD>-->
  </TR>
  {section name="sec2" loop="$wholeItemArr"}
  <TR>
    <TD align="right" NOWRAP> {math equation="buyQty-sellQty" buyQty=$wholeItemArr[sec2].buyQty sellQty=$wholeItemArr[sec2].sellQty} </TD>
    <TD align="right">{$wholeItemArr[sec2].buyQty}</TD>
    <TD align="right">{$wholeItemArr[sec2].buyRash}</TD>
    <TD>&nbsp;</TD>
    <TD align="right">{$wholeItemArr[sec2].sellQty}</TD>
    <TD align="right">{$wholeItemArr[sec2].sellRash}</TD>
    <TD align="right" colspan="2" NOWRAP>{$wholeItemArr[sec2].itemIdExpiry}</TD>
    <TD align="right" NOWRAP> {if $wholeItemArr[sec2].profitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if} <span id="whole1_{$wholeItemArr[sec2].itemId}_{$wholeItemArr[sec2].expiryDate}"
    class="total-amount-holder"
    data-item="{$wholeItemArr[sec2].itemId}"
    data-expiry="{$wholeItemArr[sec2].expiryDate}"
    >{$wholeItemArr[sec2].profitLoss}</span></FONT></TD>
    <!--<TD align="right" colspan="2">{$wholeItemArr[sec2].oneSideBrok}</TD>
    <TD align="right" NOWRAP> {if $wholeItemArr[sec2].netProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if} <span style="color:#f00" id="whole_{$wholeItemArr[sec2].itemId}_{$wholeItemArr[sec2].expiryDate}">{$wholeItemArr[sec2].netProfitLoss}</span>
      <input type="hidden" id="wholeval_{$wholeItemArr[sec2].itemId}_{$wholeItemArr[sec2].expiryDate}" value="" class="wholeprice">
      </FONT></TD>-->
  </TR>
  {/section}
  <TR>
    <TD align="right" NOWRAP> {math equation="buyQty-sellQty" buyQty=$wholeBuyQty sellQty=$wholeSellQty} </TD>
    <TD align="right">{$wholeBuyQty}</TD>
    <TD align="right">{$wholeBuyRash}</TD>
    <TD>&nbsp;</TD>
    <TD align="right">{$wholeSellQty}</TD>
    <TD align="right">{$wholeSellRash}</TD>
    <TD align="right" colspan="2" NOWRAP> : Total : </TD>
    <TD align="right" NOWRAP> {if $wholeProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if} <span id="wholepl1">{$wholeNetProfitLoss}</span></FONT></TD>
    <!--<TD align="right" colspan="2"> {$wholeOneSideBrok}<br />
      {$wholeBrok1}<br />
      {$wholeBrok2} </TD>
    <TD align="right" NOWRAP> {if $wholeNetProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if} <span id="wholepl">{$wholeNetProfitLoss}</span></FONT></TD>-->
  </TR>
</form>
</TABLE>
</BODY>
</HTML>
