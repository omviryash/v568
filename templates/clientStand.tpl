<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: NORMAL}
{/literal}
</STYLE>  
</HEAD>
<BODY>
<TABLE border="1" cellPadding="2" cellSpacing="0">
<TR>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Buy</TD>
  <TD colspan="2" align="center">Sell</TD>
  <TD colspan="6">&nbsp;</TD>
</TR>
<TR>
  <TD align="center">BuySell</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Date</TD>
  <TD align="center">Stand</TD>
  <TD align="center">Item</TD>
  <TD align="center">Vendor</TD>
  <TD colspan="2" align="center">Name</TD>
</TR>
{section name="sec1" loop="$trades"}
<TR style="color:{$trades[sec1].fontColor}">
  <TD align="center">{$trades[sec1].buySell}</TD>
  <TD align="right">{$trades[sec1].buyQty}</TD>
  <TD align="right">{$trades[sec1].price}</TD>
  <TD align="right">{$trades[sec1].sellQty}</TD>
  <TD align="right">{$trades[sec1].sellPrice}</TD>
  <TD NOWRAP>{$trades[sec1].tradeDate}</TD>
  <TD>{$trades[sec1].standing}</TD>
  <TD align="center" NOWRAP>{$trades[sec1].itemIdExpiry}</TD>
  <TD>{$trades[sec1].vendor}</TD>
  <TD colspan="2">{$trades[sec1].clientId} : {$trades[sec1].clientName}</TD>
</TR>
{/section}

</TABLE>
</BODY>
</HTML>
