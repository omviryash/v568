<LINK rel="stylesheet" type="text/css" href="./menuFiles/ddsmoothmenu.css" />
<SCRIPT type="text/javascript" src="./menuFiles/jquery.min.js"></SCRIPT>
<SCRIPT type="text/javascript" src="./menuFiles/ddsmoothmenu.js"></SCRIPT>
<DIV id="smoothmenu1" class="ddsmoothmenu">
<UL>

<LI><A href="#">Masters</A></LI>
  <UL>
    <LI><A href="brokerage.php">Items</A></LI>
    <LI><A href="expiryList.php">Expiry Dates</A></LI>
    <LI><A href="clientList.php">Clients and Brokers</A></LI>
    <LI><A href="bankMasterList.php">Bank Masters</A></LI>
    <LI><A href="otherExpAdd.php">Other Expense</A></LI>
  </UL>
</LI>

<LI><A href="#">Trades</A>
  <UL>
    <LI><A href="#">F_O Trades</A>
    	<UL>
        <LI><A href="clientTradesPer2side2fo.php">Client F_O Trades</A></LI>
		    <LI><A href="brokerTradesPer2side2fo.php">Broker F_O Trades</A></LI>
        <LI><A href="clientTradesPer2side2foHigh.php">Client F_O Trades High</A></LI>
<!--        <LI><A href="clientTradesPer2side2BothF_O.php">Client F_O Trades 2side2Both</A></LI>
		    <LI><A href="brokerTradesPer2side2BothF_O.php">Broker F_O Trades 2side2Both</A></LI>
        <LI><A href="clientTradesPer2side2foHigh.php">Client F_O Trades High</A></LI>
        <LI><A href="clientTradesPer1sideF_O.php">Client F_O Trades 1 Side</A></LI>
		    <LI><A href="brokerTradesPer1sideF_O.php">Broker F_O Trades 1 Side</A></LI>
    	  <LI><A href="clientTradesF_O.php"        >Client F_O Trades In Rs</A></LI>-->
    	</UL>
    </LI>
    <LI><A href="selectDtSession.php?goTo=clientTrades">Detailed Trades</A></LI>
    <LI><A href="txtFile.php">Store TWS Trades</A></LI>
    <LI><A href="txtFileOdin.php">Store ODIN Trades</A></LI>
    <LI><A href="#">&nbsp;</A></LI>
    <?php
      if($tradeInNewWindow == 1)
      {
    ?>
        <LI><A href="" onClick="tradeWindow=window.open('ajxStoreLimitTradeMcx1.php?useTpl=txtbx&exchange=F_O', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=120, left=1, top=300'); return false;">Data Entry F_O ClientId</A></LI>
        <LI><A href="" onClick="tradeWindow=window.open('addTrade.php?exchange=F_O', 'tradeWindow','toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=120, left=1, top=300'); return false;">Data Entry F_O New</A></LI>
    <?php
      }
      else
      {
    ?>
        <LI><A href="ajxStoreLimitTradeMcx1.php?useTpl=txtbx&exchange=F_O">Data Entry F_O ClientId</A></LI>
        <LI><A href="addTrade.php?exchange=F_O">Data Entry F_O New</A></LI>
    <?php
      }
    ?>
  </UL>
</LI>

<LI><A href="#">Other Options</A>
  <UL>
    <LI><A href="tradeListForCrossChecking.php">Confirm</A></LI>
    <LI><A href="orderList.php">Order List</A></LI>
    <LI><A href="attachNameToTxtData.php">Pending Trades</A></LI>
    <LI><A href="attachNameToTxtDataAll.php">Change Name</A></LI>
    <LI><A href="deleteAll.php">Delete Trades</A></LI>
  </UL>
</LI>

<LI><A href="#">Settlement</A>
  <UL>
    <LI><A href="mnuStand.php"                                   >Standing</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=gross"      >Generate Bill F_O</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=tradesPrint">Bill Print F_O</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=tradesPrintOnlyNet">Bill Print F_O Only Net</A></LI>
    <LI><A href="#">Rollback</A></LI>
  </UL>
</LI>

<LI><A href="#">Reports</A>
  <UL>
    <LI><A href="clientTradesPer2side2fo.php?display=itemPending">Script Outstanding F_O</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=itemPending2">Average Outstanding F_O</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=itemWiseGross">Script Gross F_O</A></LI>
  </UL>
</LI>

<LI><A href="#">Accounts</A>
  <UL>
    <LI><A href="accSummary.php">Account Last Balance</A></LI>
    <LI><A href="mnuAccount.php">Money Transaction</A></LI>
  </UL>
</LI>

<LI><A href="#">Utilities</A>
  <UL>
    <LI><A href="writeTradesFile.php">Online Backup</A></LI>
    <LI><A href="backup.php">Backup</A></LI>
    <LI><A href="links.php">Useful Links</A></LI>
    <LI><A href="calc.php">Calculator</A></LI>
    <LI><A href="changePassword.php">Change Password</A></LI>
    <LI><A href="#">ExtraMenu</A>
      <UL>
        <LI><A href="ajxStoreLimitTradeFO.php">Data Entry FO Fast</A></LI>
        <LI><A href="storeLimitTradeF_O.php">Data Entry F_O Slow</A></LI>
      </UL>
    </LI>
  </UL>
</LI>

<LI><A href="selectDtSession.php">Date Range</A>
</LI>
<LI><A href="logout.php">Logout</A>
</LI>

</UL>
<BR style="clear: left" />
</DIV>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>