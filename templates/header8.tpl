<LINK rel="stylesheet" type="text/css" href="./menuFiles/ddsmoothmenu.css" />
<SCRIPT type="text/javascript" src="./menuFiles/jquery.min.js"></SCRIPT>
<SCRIPT type="text/javascript" src="./menuFiles/ddsmoothmenu.js"></SCRIPT>
<DIV id="smoothmenu1" class="ddsmoothmenu">
<UL>

<LI><A href="#">Masters</A></LI>
  <UL>
    <LI><A href="brokerage.php">Items</A></LI>
    <LI><A href="expiryList.php">Expiry Dates</A></LI>
    <LI><A href="clientList.php">Clients and Brockers</A></LI>
    <LI><A href="bankMasterAdd.php">Bank Masters</A></LI>
    <LI><A href="otherExpAdd.php">Other Expence</A></LI>
    <LI><A href="exchangeAdd.php">Exchange</A></LI>
  </UL>
</LI>

<LI><A href="#">Trades</A></LI>
  <UL>
    <LI><A href="storeLimitTradeMcx.php">Data Entry Mcx</A></LI>
    <LI><A href="storeLimitTradeF_O.php">Data Entry F_O</A></LI>
    <LI><A href="clientTradesMcx.php">Mcx Trades</A></LI>
    <LI><A href="clientTradesPer2side2fo.php">F_O Trades</A></LI>
    <LI><A href="tradeListForCrossChecking.php">Confirm</A></LI>
    <LI><A href="orderList.php">Order List</A></LI>
    <LI><A href="selectDtSession.php?goTo=clientTrades">Print</A></LI>
    <LI><A href="selectDtSession.php?goTo=clientTrades">Detailed Trades</A></LI>
    <LI><A href="txtFile.php">Store TWS Trades</A></LI>
    <LI><A href="txtFileOdin.php">Store ODIN Trades</A></LI>
    <LI><A href="ajxStoreLimitTradeMcx.php">Data Entry Mcx Fast</A></LI>
  </UL>
</LI>

<LI><A href="#">Other Options</A>
  <UL>
    <LI><A href="attachNameToTxtData.php">Pending Trades</A></LI>
    <LI><A href="attachNameToTxtDataAll.php">Change Name</A></LI>
    <LI><A href="deleteAll.php">Delete Trades</A></LI>
  </UL>
</LI>

<LI><A href="#">Delivery</A>
  <UL>
    <LI><A href="#">Allocations</A>
      <UL>
        <LI><A href="#">Mark Full Delivery</A></li>
        <LI><A href="#">Remove Marking</A></li>
      </UL>
    </LI>
    <LI><A href="#">Delivery List</A>
      <UL>
        <LI><A href="#">Party wise</A></li>
        <LI><A href="#">Script wise</A></li>
      </UL>
    </LI>
  </UL>
</LI>

<LI><A href="#">Settlement</A>
  <UL>
    <LI><A href="clientTradesMcx.php?display=gross"              >Generate Bill MCX</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=gross"      >Generate Bill F_O</A></LI>
    <LI><A href="clientTradesMcx.php?display=tradesPrint"        >Bill Print MCX</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=tradesPrint">Bill Print F_O</A></LI>
    <LI><A href="#">Rollback</A></LI>
  </UL>
</LI>

<LI><A href="#">Reports</A>
  <UL>
    <LI><A href="clientTradesMcx.php?display=itemPending">Script Outstanding MCX</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=itemPending">Script Outstanding F_O</A></LI>
    <LI><A href="clientTradesMcx.php?display=itemPending2">Average Outstanding MCX</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=itemPending2">Average Outstanding F_O</A></LI>
    <LI><A href="clientTradesMcx.php?display=itemWiseGross">Script Gross MCX</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=itemWiseGross">Script Gross F_O</A></LI>
    <LI><A href="clientTradesMcx.php?display=gross">Bill Gross MCX</A></LI>
    <LI><A href="clientTradesPer2side2fo.php?display=gross">Bill Gross F_O</A></LI>
  </UL>
</LI>

<LI><A href="#">Accounts</A>
  <UL>
    <LI><A href="accSummary.php">Account Last Balance</A></LI>
    <LI><A href="mnuAccount.php">Money Transaction</A></LI>
  </UL>
</LI>

<LI><A href="#">Utilities</A>
  <UL>
    <LI><A href="writeTradesFile.php">Online Backup</A></LI>
    <LI><A href="http://localhost/pma/db_details_export.php?lang=en-utf-8&server=1&collation_connection=utf8_general_ci&db=ommcx&goto=db_details_export.php&selectall=1">Backup</A></LI>
    <LI><A href="links.php">Useful Links</A></LI>
    <LI><A href="calc.php">Calculator</A></LI>
    <LI><A href="changePassword.php">Change Password</A></LI>
  </UL>
</LI>

<LI>
  <A href="selectDtSession.php">Date Range</A>
</LI>

<LI>
  <A href="logout.php">Logout</A>
</LI>

</UL>
<BR style="clear: left" />
</DIV>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>