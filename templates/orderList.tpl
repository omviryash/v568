<HTML>
<HEAD><TITLE>Order List</TITLE>
{literal}
<SCRIPT language = "javascript">
function conf(orderId)
{
	if(confirm("Are You Sure You want to Delete Record?"))
			location.href="deleteOrder.php?orderId="+orderId;
	else
  		location.href="orderList.php";
}

function cancelOrder(orderId)
{
	if(confirm("Are You Sure You want to Cancel Order?"))
			location.href="cancelOrder.php?orderId="+orderId;
}

function writeOrder(orderId)
{
	if(confirm("Are You Sure You want to Pass Order?"))
			location.href="writeOrderToTradetxt.php?orderId="+orderId;
}

function change()
 {
  var select1value= document.form1.cboItem.value;
  var select2value=document.form1.cboExpiryDate;
  select2value.options.length=0;
  {/literal}
   {section name=sec1 loop=$k}
    if( select1value=="{$itemId[sec1]}")
  	{literal}{{/literal}
  		{section name=sec2 loop=$l+10}
        {if $expiryDate[sec1][sec2] neq ""}
          select2value.options[{$smarty.section.sec2.index}]=new Option("{$expiryDate[sec1][sec2]}","{$expiryDate[sec1][sec2]}"); 
        {/if}
        {if $expiryDate[sec1][sec2] eq "$expiryDateBack"}
          select2value.options[{$smarty.section.sec2.index}].selected=true; 
        {/if}
      {/section}
    {literal}}{/literal}
  {/section}
  }
</SCRIPT>
</HEAD>
{if $listOnly eq "0"}
<BODY onLoad="change();">
{else}
<BODY onKeyPress="if(event.keyCode==27)  window.close();">
{/if}
  <FORM name="form1" action="" method="POST">
{if $listOnly eq "0"}
<A href="./index.php">Home</A><BR><BR>
  Client:
  <SELECT name="cboClientName">
    <option value="All">All</option>
    {html_options values="$clientId" output="$clientName" selected="$clientNameBack"}
  </SELECT>
  Item:
  <SELECT name="cboItem" onChange="change();">
    <option value="All">All</option>
    {html_options values="$itemId" output="$itemId" selected="$itemBack"}
  </SELECT>
  Expiry Date:
  <SELECT name="cboExpiryDate">
    <option value="All">All</option>
  </SELECT>
  Order Status:
  <SELECT name="cboStatus">
    <option value="All">All</option>
    {if $orderStatusBack eq "Pending"}
      <option value="Pending" selected=selected>Pending</option>
    {else}
      <option value="Pending">Pending</option>
    {/if}

    {if $orderStatusBack eq "Cancel"}
      <option value="Cancel" selected=selected>Cancel</option>
    {else}
      <option value="Cancel">Cancel</option>
    {/if}

    {if $orderStatusBack eq "Executed"}
      <option value="Executed" selected=selected>Executed</option>
    {else}
      <option value="Executed">Executed</option>
    {/if}

  </SELECT><BR>
  Order Type:
  <SELECT name="cboOrderType">
    <option value="All">All</option>
    {if $orderTypeBack eq "RL"}
      <option value="RL" selected=selected>RL</option>
    {else}
      <option value="RL">RL</option>
    {/if}
    
    {if $orderTypeBack eq "SL"}
      <option value="SL" selected=selected>SL</option>
    {else}
      <option value="SL">SL</option>
    {/if}
  </SELECT>
  Order Validity:
  <SELECT name="cboOrderValidity">
    <option value="All">All</option>
    {if $orderValidityBack eq "EOS"}
      <option value="EOS" selected=selected>EOS</option>
    {else}
      <option value="EOS">EOS</option>
    {/if}
    
    {if $orderValidityBack eq "GTD"}
      <option value="GTD" selected=selected>GTD</option>
    {else}
      <option value="GTD">GTD</option>
    {/if}
    
    {if $orderValidityBack eq "GTC"}
      <option value="GTC" selected=selected>GTC</option>
    {else}
      <option value="GTC">GTC</option>
    {/if}
  </SELECT><BR>
  From: {html_select_date prefix="toDate" time=$dateTo start_year="+1" end_year="-1" day_value_format=%02d field_order="dmy"}
  To: {html_select_date prefix="fromDate" time=$dateFrom start_year="+1" end_year="-1" day_value_format=%02d field_order="dmy"}
  <INPUT type="submit" name="btnSubmit" value="Go!!"><BR><BR>
{/if}
  <HR>
  
  <TABLE border="1" cellSpacing="0" cellPadding="1">
    <!--<TR>
      <TH colspan="13">ORDER LIST</TH>
    </TR>-->
    <TR style="font-size: 14;" align="center" bgcolor="pink">
      <TD>Delete</TD>
      <TD>Cancel</TD>
      <TD>Date</TD>
      <TD>Name</TD>
      <TD>Item</TD>
      <TD>Expiry Date</TD>
      <TD>B/S</TD>
      <TD>Qty</TD>
      <TD>Price</TD>
      <TD>Trig</TD>
      <TD>Status</TD>
      <TD>Order Type</TD>
      <TD>Validity</TD>
      <TD>Remarks</TD>
      <TD>Vendor</TD>
      <TD>Valid Till Date</TD>
    </TR>
    {if $i neq "0"}
    {section name="sec1" loop=$i}
      {if $buySell[sec1] eq "Buy"}
        <TR style="color:blue">
      {else}
        <TR style="color:red">
      {/if}
      <TD><A href="javascript: conf({$orderId[sec1]});">Delete</A></TD>
      {if $orderStatus[sec1] neq "Executed" and $orderStatus[sec1] neq "Cancel"}
        <TD><A href="javascript: cancelOrder({$orderId[sec1]});">Cancel</A></TD>
        <TD><A href="javascript: writeOrder({$orderId[sec1]});">{$orderDate[sec1]}</A> {$orderTime[sec1]}</TD>
      {else}
        <TD align="center">-</TD>
        <TD NOWRAP>{$orderDate[sec1]} {$orderTime[sec1]}</TD>
      {/if}
      
      <TD align="center">{$clientNameSelected[sec1]}</TD>
      <TD align="center">{$itemSelected[sec1]}</TD>
      <TD>{$expiryDateSelected[sec1]}</TD>
      <TD>{$buySell[sec1]}</TD>
      <TD ALIGN="RIGHT">{$qty[sec1]}</TD>
      <TD ALIGN="RIGHT">
        {if $orderStatus[sec1] eq 'Pending'}
        <A href='' onClick="tradeWindow=window.open('tradeEdit.php?orderId={$orderId[sec1]}', 'tradeWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=130, left=1, top=300'); return false;">
        {/if}
          {$price[sec1]}
         {if $orderStatus[sec1] eq 'Pending'}
         </A>
         {/if}
      </TD>
      <TD align="center">{$triggerPrice[sec1]}</TD>
      <TD align="center">{$orderStatus[sec1]}</TD>
      <TD align="center">{$orderType[sec1]}</TD>
      <TD align="center">{$orderValidity[sec1]}</TD>
      <TD>{$userRemarks[sec1]}</TD>
      <TD>{$vendor[sec1]}</TD>
      <TD>{$orderValidTillDate[sec1]}</TD>
    </TR>
    {/section}
    {/if}
    
    {if $message eq "No Records Found"}
    <TR>
      <TH colspan="15">{$message}</TH>
    </TR>
    {else}
    <TR>
      <TD colspan="15">Total Rows Selected = {$smarty.section.sec1.index}</TD>
    </TR>
    {/if}
  </TABLE>
<TABLE border="1" cellPadding="0" cellSpacing="0" width="100%">
<TR bgcolor="pink" align="center">
<TD>Item</TD>
<TD NOWRAP>Expiry Date</TD>
<TD>Pending</TD>
<TD>Cancel</TD>
<TD NOWRAP>Executed</TD>
<TD NOWRAP>BQty</TD>
<TD NOWRAP>SQty</TD>
<TD NOWRAP>NetQty</TD>
<TD NOWRAP>AvgB Price</TD>
<TD NOWRAP>AvgS Price</TD>
<TD>RL</TD>
<TD>SL</TD>
<TD>EOS</TD>
<TD>GTD</TD>
<TD>GTC</TD>
</TR>

  {section name="sec4" loop=$num1+1}
    <TR>
    <TD>{$rowItem[sec4]}</TD>
    <TD>{$rowExpiryDate[sec4]}</TD>
    <TD ALIGN="RIGHT">{$pending[sec4]}</TD>
    <TD ALIGN="RIGHT">{$canceled[sec4]}</TD>
    <TD ALIGN="RIGHT">{$executed[sec4]}</TD>
    <TD ALIGN="RIGHT">{$buyQty[sec4]}</TD>
    <TD ALIGN="RIGHT">{$sellQty[sec4]}</TD>
    <TD ALIGN="RIGHT">{math equation="(x-y)" x=$buyQty[sec4] y=$sellQty[sec4]}</TD>
    <TD ALIGN="RIGHT">-</TD>
    <TD ALIGN="RIGHT">-</TD>
    <TD ALIGN="RIGHT">{$rl[sec4]}</TD>
    <TD ALIGN="RIGHT">{$sl[sec4]}</TD>
    <TD ALIGN="RIGHT">{$eos[sec4]}</TD>
    <TD ALIGN="RIGHT">{$gtd[sec4]}</TD>
    <TD ALIGN="RIGHT">{$gtc[sec4]}</TD>
</TR>    
  {/section}
</TABLE>
</FORM>
</BODY>
</HTML>