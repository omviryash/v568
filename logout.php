<?php
  session_start();
  session_unregister('user');
  session_unregister('userType');
  session_destroy(); 
  header("Location: login.php");
?>
