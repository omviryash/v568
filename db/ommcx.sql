-- phpMyAdmin SQL Dump
-- version 4.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 27, 2015 at 07:25 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `v568`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankmaster`
--

CREATE TABLE IF NOT EXISTS `bankmaster` (
`bankId` int(6) NOT NULL,
  `bankName` varchar(60) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `phone1` varchar(12) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `phone2` varchar(12) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bhavcopy`
--

CREATE TABLE IF NOT EXISTS `bhavcopy` (
`bhavcopyid` int(10) NOT NULL,
  `exchange` varchar(30) NOT NULL DEFAULT '',
  `bhavcopyDate` date NOT NULL DEFAULT '0000-00-00',
  `sessionId` varchar(15) NOT NULL DEFAULT '',
  `marketType` varchar(15) NOT NULL DEFAULT '',
  `instrumentId` int(10) NOT NULL DEFAULT '0',
  `instrumentName` varchar(15) NOT NULL DEFAULT '',
  `scriptCode` int(10) NOT NULL DEFAULT '0',
  `contractCode` varchar(20) NOT NULL DEFAULT '',
  `scriptGroup` varchar(5) NOT NULL DEFAULT '',
  `scriptType` varchar(5) NOT NULL DEFAULT '',
  `expiryDate` date NOT NULL DEFAULT '0000-00-00',
  `expiryDateBc` varchar(10) NOT NULL DEFAULT '',
  `strikePrice` float NOT NULL DEFAULT '0',
  `optionType` varchar(4) NOT NULL DEFAULT '',
  `previousClosePrice` float NOT NULL DEFAULT '0',
  `openPrice` float NOT NULL DEFAULT '0',
  `highPrice` float NOT NULL DEFAULT '0',
  `lowPrice` float NOT NULL DEFAULT '0',
  `closePrice` float NOT NULL DEFAULT '0',
  `totalQtyTrade` int(10) NOT NULL DEFAULT '0',
  `totalValueTrade` double NOT NULL DEFAULT '0',
  `lifeHigh` float NOT NULL DEFAULT '0',
  `lifeLow` float NOT NULL DEFAULT '0',
  `quoteUnits` varchar(10) NOT NULL DEFAULT '',
  `settlementPrice` float NOT NULL DEFAULT '0',
  `noOfTrades` int(6) NOT NULL DEFAULT '0',
  `openInterest` double NOT NULL DEFAULT '0',
  `avgTradePrice` float NOT NULL DEFAULT '0',
  `tdcl` float NOT NULL DEFAULT '0',
  `lstTradePrice` float NOT NULL DEFAULT '0',
  `remarks` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cashflow`
--

CREATE TABLE IF NOT EXISTS `cashflow` (
`cashFlowId` int(6) NOT NULL,
  `clientId` int(10) NOT NULL DEFAULT '0',
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `dwStatus` char(2) NOT NULL DEFAULT '',
  `dwAmount` double NOT NULL DEFAULT '0',
  `plStatus` char(2) NOT NULL DEFAULT '',
  `plAmount` double NOT NULL DEFAULT '0',
  `transactionDate` date DEFAULT NULL,
  `transType` varchar(20) DEFAULT NULL,
  `transMode` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `exchange` varchar(20) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
`clientId` int(6) NOT NULL,
  `passwd` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `openingDate` date DEFAULT NULL,
  `opening` float DEFAULT NULL,
  `deposit` int(6) DEFAULT NULL,
  `currentBal` float DEFAULT NULL,
  `address` text,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(22) DEFAULT NULL,
  `fax` varchar(10) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `oneSide` tinyint(1) DEFAULT '0',
  `remiser` int(6) DEFAULT '0',
  `remiserBrok` float DEFAULT '0',
  `remiserBrokIn` tinyint(1) DEFAULT '1',
  `clientBroker` int(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clientbrok`
--

CREATE TABLE IF NOT EXISTS `clientbrok` (
`clientBrokId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `exchange` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `oneSideBrok` float DEFAULT NULL,
  `brok1` float DEFAULT NULL,
  `brok2` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clientexchange`
--

CREATE TABLE IF NOT EXISTS `clientexchange` (
`clientexchangeId` int(6) NOT NULL,
  `clientId` int(6) NOT NULL DEFAULT '0',
  `exchange` varchar(50) NOT NULL DEFAULT '',
  `brok` int(50) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exchange`
--

CREATE TABLE IF NOT EXISTS `exchange` (
`exchangeId` int(6) unsigned NOT NULL,
  `exchange` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `multiply` tinyint(1) NOT NULL DEFAULT '0',
  `profitBankRate` float DEFAULT NULL,
  `lossBankRate` float DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exchange`
--

INSERT INTO `exchange` (`exchangeId`, `exchange`, `multiply`, `profitBankRate`, `lossBankRate`) VALUES
(1, 'MCX', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `expensemaster`
--

CREATE TABLE IF NOT EXISTS `expensemaster` (
`expensemasterId` int(6) NOT NULL,
  `expenseName` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expiry`
--

CREATE TABLE IF NOT EXISTS `expiry` (
`expiryId` int(6) NOT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `exchange` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=984 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expiry`
--

INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES
(978, 'SILVER', '03JUL2015', 'MCX'),
(982, 'GOLDM', '05AUG2015', 'MCX'),
(979, 'SILVERM', '03JUL2015', 'MCX'),
(977, 'LEAD', '29MAY2015', 'MCX'),
(981, 'CRUDEOIL', '19JUN2015', 'MCX'),
(974, 'COPPER', '30JUN2015', 'MCX'),
(975, 'NICKEL', '29MAY2015', 'MCX'),
(976, 'ZINC', '29MAY2015', 'MCX'),
(983, 'GOLD', '05AUG2015', 'MCX'),
(973, 'NATURALGAS', '26MAY2015', 'MCX');

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE IF NOT EXISTS `general` (
`generalId` int(6) NOT NULL,
  `filePath` varchar(250) DEFAULT NULL,
  `fileName` varchar(200) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general`
--

INSERT INTO `general` (`generalId`, `filePath`, `fileName`) VALUES
(1, 'bhavcopies', '');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `itemId` varchar(50) NOT NULL DEFAULT '',
  `item` varchar(50) DEFAULT NULL,
  `itemShort` varchar(50) DEFAULT NULL,
  `brok` float DEFAULT NULL,
  `brok2` float DEFAULT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `min` int(6) DEFAULT NULL,
  `priceOn` int(6) DEFAULT NULL,
  `mulAmount` float DEFAULT '1',
  `rangeStart` float DEFAULT NULL,
  `rangeEnd` float DEFAULT NULL,
  `qtyInLots` tinyint(1) DEFAULT NULL,
  `exchangeId` int(6) unsigned NOT NULL DEFAULT '0',
  `exchange` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `multiply` float NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchangeId`, `exchange`, `multiply`) VALUES
('COPPER', 'COPPER', 'COPPER', 0, 0, 250, 1000, 1, 1, 300, 550, NULL, 1, 'MCX', 0),
('CRUDEOIL', 'CRUDEOIL', 'CRUDEOIL', 0, 0, 250, 100, 1, 1, 2500, 8000, NULL, 1, 'MCX', 0),
('SILVER', 'SILVER', 'SILVER', 0, 0, 250, 30, 1, 1, 30000, 65000, NULL, 1, 'MCX', 0),
('GOLDM', 'GOLDM', 'GOLDM', 100, 100, 70, 10, 1, 1, 0, 0, NULL, 1, 'MCX', 0),
('NICKEL', 'NICKEL', 'NICKEL', 0, 0, 250, 250, 1, 1, 750, 1300, NULL, 1, 'MCX', 0),
('ZINC', 'ZINC', 'ZINC', 0, 0, 250, 5000, 1, 1, 120, 145, NULL, 1, 'MCX', 0),
('SILVERM', 'SILVERM', 'SILVERM', 0, 0, 70, 5, 1, 1, 0, 0, NULL, 1, 'MCX', 0),
('GOLD', 'GOLD', 'GOLD', 0, 0, 250, 100, 1, 1, 20000, 35000, NULL, 1, 'MCX', 0),
('LEAD', 'LEAD', 'LEAD', 0, 0, 250, 5000, 1, 1, 98, 140, NULL, 1, 'MCX', 0),
('NATURALGAS', 'NATURALGAS', 'NATURALGAS', 0, 0, 150, 1250, 1, 1, 150, 280, NULL, 1, 'MCX', 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
`menuId` int(10) unsigned NOT NULL,
  `fileToOpen` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `title` varchar(55) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `displayToAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `displayToOperator` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `displayToClient` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `newWindow` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `newWindowName` varchar(20) NOT NULL DEFAULT '',
  `newWindowPerameter` text CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newexpmaster`
--

CREATE TABLE IF NOT EXISTS `newexpmaster` (
`newExpMasterId` int(6) NOT NULL,
  `newExpName` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`orderId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `clientId2` int(6) DEFAULT NULL,
  `firstName2` varchar(35) CHARACTER SET utf8 DEFAULT NULL,
  `middleName2` varchar(35) CHARACTER SET utf8 DEFAULT NULL,
  `lastName2` varchar(35) CHARACTER SET utf8 DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `orderTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `price2` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `orderRefNo` varchar(60) DEFAULT NULL,
  `orderNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `orderType` varchar(30) NOT NULL DEFAULT '',
  `orderValidity` varchar(15) NOT NULL DEFAULT '',
  `orderValidTillDate` date NOT NULL DEFAULT '0000-00-00',
  `orderStatus` varchar(30) NOT NULL DEFAULT '',
  `triggerPrice` float DEFAULT NULL,
  `exchange` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `refOrderId` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orderId`, `clientId`, `firstName`, `middleName`, `lastName`, `clientId2`, `firstName2`, `middleName2`, `lastName2`, `buySell`, `itemId`, `orderDate`, `orderTime`, `qty`, `price`, `price2`, `brok`, `orderRefNo`, `orderNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `orderType`, `orderValidity`, `orderValidTillDate`, `orderStatus`, `triggerPrice`, `exchange`, `refOrderId`) VALUES
(59, 22, 'S.T', '', '', 1, 'ZDP', '', '', 'Sell', 'SILVER', '2013-03-19', '14:50:19', 30, 54420, 54420, NULL, NULL, NULL, '04MAY2013', '_SELF', NULL, NULL, 'RL', '', '0000-00-00', 'Pending', NULL, 'MCX', 60),
(60, 1, 'ZDP', '', '', 22, 'S.T', '', '', 'Buy', 'SILVER', '2013-03-19', '14:50:19', 30, 54420, 54420, NULL, NULL, NULL, '04MAY2013', '_SELF', NULL, NULL, 'RL', '', '0000-00-00', 'Pending', NULL, 'MCX', 59);

-- --------------------------------------------------------

--
-- Table structure for table `otherexp`
--

CREATE TABLE IF NOT EXISTS `otherexp` (
`otherexpId` int(6) NOT NULL,
  `otherExpName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `otherExpDate` date DEFAULT NULL,
  `otherExpAmount` float DEFAULT NULL,
  `note` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `otherExpMode` varchar(60) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `partybrokerage`
--

CREATE TABLE IF NOT EXISTS `partybrokerage` (
`partybrokerageId` int(11) NOT NULL,
  `partyId` int(11) DEFAULT NULL,
  `brokerageDate` date DEFAULT NULL,
  `brokerage` float NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
`settingsId` int(6) NOT NULL,
  `settingsKey` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `value` varchar(60) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settingsId`, `settingsKey`, `value`) VALUES
(1, 'uploadFileWorks', '1'),
(2, 'expiryDisplay', 'monthOnly'),
(3, 'profitBankRate', '44'),
(4, 'lossBankRate', '44.50'),
(5, 'clientFieldInTxt', 'ownClient'),
(6, 'clientFieldInTxt', 'userRemarks'),
(7, 'takeTimeEntry', '0'),
(8, 'takeTradeNote', '0'),
(9, 'qtyInLots', '0'),
(10, 'useItemPriceRange', '1'),
(11, 'odinTxtFilePath', 'C:\\ODIN\\DIET\\OnLineBackup\\MCX\\Trades'),
(12, 'billWithLedger', '1');

-- --------------------------------------------------------

--
-- Table structure for table `standing`
--

CREATE TABLE IF NOT EXISTS `standing` (
`standingId` int(6) NOT NULL,
  `standingDtCurrent` date DEFAULT NULL,
  `standingDtNext` date DEFAULT NULL,
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `standingPrice` float DEFAULT NULL,
  `exchange` varchar(20) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `storedbhav`
--

CREATE TABLE IF NOT EXISTS `storedbhav` (
  `stordId` int(11) NOT NULL DEFAULT '0',
  `storDate` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `status` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxt`
--

CREATE TABLE IF NOT EXISTS `tradetxt` (
`tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `clientId2` int(6) DEFAULT NULL,
  `firstName2` varchar(35) CHARACTER SET utf8 DEFAULT NULL,
  `middleName2` varchar(35) CHARACTER SET utf8 DEFAULT NULL,
  `lastName2` varchar(35) CHARACTER SET utf8 DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `price2` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) DEFAULT '0',
  `exchange` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `refTradeId` int(10) DEFAULT NULL,
  `selfRefId` int(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxtv1`
--

CREATE TABLE IF NOT EXISTS `tradetxtv1` (
`tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `clientId` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `deletepassword` varchar(50) NOT NULL DEFAULT '',
  `userType` varchar(20) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`clientId`, `name`, `password`, `deletepassword`, `userType`) VALUES
('', 'om', 'jp11111', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE IF NOT EXISTS `vendor` (
`vendorId` int(6) NOT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `address` text,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(22) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `deposit` float DEFAULT NULL,
  `currentBal` float DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`vendorId`, `vendor`, `firstName`, `middleName`, `lastName`, `address`, `phone`, `mobile`, `fax`, `email`, `deposit`, `currentBal`) VALUES
(6, '_SELF', '_SELF', '', '', '', '', '', '', '', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendorbrok`
--

CREATE TABLE IF NOT EXISTS `vendorbrok` (
`clientBrokId` int(6) NOT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `oneSideBrok` int(6) DEFAULT NULL,
  `brok1` float DEFAULT NULL,
  `brok2` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendortrades`
--

CREATE TABLE IF NOT EXISTS `vendortrades` (
`tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxexpiry`
--

CREATE TABLE IF NOT EXISTS `zcxexpiry` (
`expiryId` int(6) NOT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxitem`
--

CREATE TABLE IF NOT EXISTS `zcxitem` (
  `itemId` varchar(10) NOT NULL DEFAULT '',
  `item` varchar(50) DEFAULT NULL,
  `oneSideBrok` float DEFAULT '0',
  `mulAmount` float DEFAULT '0',
  `minQty` float DEFAULT NULL,
  `brok` int(6) DEFAULT '1',
  `brok2` int(6) DEFAULT '1',
  `per` int(6) DEFAULT '1',
  `unit` int(6) DEFAULT '1',
  `min` int(6) DEFAULT '1',
  `priceOn` int(6) DEFAULT '1',
  `priceUnit` int(6) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxmember`
--

CREATE TABLE IF NOT EXISTS `zcxmember` (
`zCxMemberId` int(6) NOT NULL,
  `userId` varchar(10) DEFAULT NULL,
  `memberId` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxtrades`
--

CREATE TABLE IF NOT EXISTS `zcxtrades` (
`tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(10) DEFAULT NULL,
  `removeFromAccount` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bankmaster`
--
ALTER TABLE `bankmaster`
 ADD PRIMARY KEY (`bankId`);

--
-- Indexes for table `bhavcopy`
--
ALTER TABLE `bhavcopy`
 ADD PRIMARY KEY (`bhavcopyid`);

--
-- Indexes for table `cashflow`
--
ALTER TABLE `cashflow`
 ADD PRIMARY KEY (`cashFlowId`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
 ADD PRIMARY KEY (`clientId`);

--
-- Indexes for table `clientbrok`
--
ALTER TABLE `clientbrok`
 ADD PRIMARY KEY (`clientBrokId`);

--
-- Indexes for table `clientexchange`
--
ALTER TABLE `clientexchange`
 ADD PRIMARY KEY (`clientexchangeId`);

--
-- Indexes for table `exchange`
--
ALTER TABLE `exchange`
 ADD PRIMARY KEY (`exchangeId`);

--
-- Indexes for table `expensemaster`
--
ALTER TABLE `expensemaster`
 ADD PRIMARY KEY (`expensemasterId`);

--
-- Indexes for table `expiry`
--
ALTER TABLE `expiry`
 ADD PRIMARY KEY (`expiryId`);

--
-- Indexes for table `general`
--
ALTER TABLE `general`
 ADD PRIMARY KEY (`generalId`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
 ADD PRIMARY KEY (`menuId`);

--
-- Indexes for table `newexpmaster`
--
ALTER TABLE `newexpmaster`
 ADD PRIMARY KEY (`newExpMasterId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`orderId`);

--
-- Indexes for table `otherexp`
--
ALTER TABLE `otherexp`
 ADD PRIMARY KEY (`otherexpId`);

--
-- Indexes for table `partybrokerage`
--
ALTER TABLE `partybrokerage`
 ADD PRIMARY KEY (`partybrokerageId`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
 ADD PRIMARY KEY (`settingsId`);

--
-- Indexes for table `standing`
--
ALTER TABLE `standing`
 ADD UNIQUE KEY `standingId` (`standingId`);

--
-- Indexes for table `tradetxt`
--
ALTER TABLE `tradetxt`
 ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `tradetxtv1`
--
ALTER TABLE `tradetxtv1`
 ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
 ADD PRIMARY KEY (`vendorId`);

--
-- Indexes for table `vendorbrok`
--
ALTER TABLE `vendorbrok`
 ADD PRIMARY KEY (`clientBrokId`);

--
-- Indexes for table `vendortrades`
--
ALTER TABLE `vendortrades`
 ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `zcxexpiry`
--
ALTER TABLE `zcxexpiry`
 ADD PRIMARY KEY (`expiryId`);

--
-- Indexes for table `zcxitem`
--
ALTER TABLE `zcxitem`
 ADD PRIMARY KEY (`itemId`);

--
-- Indexes for table `zcxmember`
--
ALTER TABLE `zcxmember`
 ADD PRIMARY KEY (`zCxMemberId`);

--
-- Indexes for table `zcxtrades`
--
ALTER TABLE `zcxtrades`
 ADD PRIMARY KEY (`tradeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bankmaster`
--
ALTER TABLE `bankmaster`
MODIFY `bankId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bhavcopy`
--
ALTER TABLE `bhavcopy`
MODIFY `bhavcopyid` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cashflow`
--
ALTER TABLE `cashflow`
MODIFY `cashFlowId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
MODIFY `clientId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `clientbrok`
--
ALTER TABLE `clientbrok`
MODIFY `clientBrokId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `clientexchange`
--
ALTER TABLE `clientexchange`
MODIFY `clientexchangeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `exchange`
--
ALTER TABLE `exchange`
MODIFY `exchangeId` int(6) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `expensemaster`
--
ALTER TABLE `expensemaster`
MODIFY `expensemasterId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `expiry`
--
ALTER TABLE `expiry`
MODIFY `expiryId` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=984;
--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
MODIFY `generalId` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
MODIFY `menuId` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newexpmaster`
--
ALTER TABLE `newexpmaster`
MODIFY `newExpMasterId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `orderId` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `otherexp`
--
ALTER TABLE `otherexp`
MODIFY `otherexpId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `partybrokerage`
--
ALTER TABLE `partybrokerage`
MODIFY `partybrokerageId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
MODIFY `settingsId` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `standing`
--
ALTER TABLE `standing`
MODIFY `standingId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tradetxt`
--
ALTER TABLE `tradetxt`
MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tradetxtv1`
--
ALTER TABLE `tradetxtv1`
MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
MODIFY `vendorId` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `vendorbrok`
--
ALTER TABLE `vendorbrok`
MODIFY `clientBrokId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendortrades`
--
ALTER TABLE `vendortrades`
MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxexpiry`
--
ALTER TABLE `zcxexpiry`
MODIFY `expiryId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxmember`
--
ALTER TABLE `zcxmember`
MODIFY `zCxMemberId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxtrades`
--
ALTER TABLE `zcxtrades`
MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
