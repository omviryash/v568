<?php /* Smarty version 2.6.10, created on 2014-02-03 07:32:25
         compiled from accTransOtherAdd.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_select_date', 'accTransOtherAdd.tpl', 21, false),)), $this); ?>
<HTML>
<HEAD><TITLE>Om !!!</TITLE>
  <STYLE src="./templates/styles.css"></STYLE>
</HEAD>
<BODY bgcolor="#B0D8FF">
<FORM name="form1" action="<?php echo $this->_tpl_vars['PHP_SELF']; ?>
" method=POST>
<A href="./accTransList.php">List</A>&nbsp;&nbsp;<A href="./mnuAccount.php">Menu</A><BR><BR>
<TABLE>
    <TR>
      <TD>Name</TD>
      <TD><SELECT name = "cboName">
            <?php unset($this->_sections['sec1']);
$this->_sections['sec1']['name'] = 'sec1';
$this->_sections['sec1']['loop'] = is_array($_loop=$this->_tpl_vars['fName']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec1']['show'] = true;
$this->_sections['sec1']['max'] = $this->_sections['sec1']['loop'];
$this->_sections['sec1']['step'] = 1;
$this->_sections['sec1']['start'] = $this->_sections['sec1']['step'] > 0 ? 0 : $this->_sections['sec1']['loop']-1;
if ($this->_sections['sec1']['show']) {
    $this->_sections['sec1']['total'] = $this->_sections['sec1']['loop'];
    if ($this->_sections['sec1']['total'] == 0)
        $this->_sections['sec1']['show'] = false;
} else
    $this->_sections['sec1']['total'] = 0;
if ($this->_sections['sec1']['show']):

            for ($this->_sections['sec1']['index'] = $this->_sections['sec1']['start'], $this->_sections['sec1']['iteration'] = 1;
                 $this->_sections['sec1']['iteration'] <= $this->_sections['sec1']['total'];
                 $this->_sections['sec1']['index'] += $this->_sections['sec1']['step'], $this->_sections['sec1']['iteration']++):
$this->_sections['sec1']['rownum'] = $this->_sections['sec1']['iteration'];
$this->_sections['sec1']['index_prev'] = $this->_sections['sec1']['index'] - $this->_sections['sec1']['step'];
$this->_sections['sec1']['index_next'] = $this->_sections['sec1']['index'] + $this->_sections['sec1']['step'];
$this->_sections['sec1']['first']      = ($this->_sections['sec1']['iteration'] == 1);
$this->_sections['sec1']['last']       = ($this->_sections['sec1']['iteration'] == $this->_sections['sec1']['total']);
?>
              <OPTION value=<?php echo $this->_tpl_vars['clId'][$this->_sections['sec1']['index']]; ?>
>
                <?php echo $this->_tpl_vars['fName'][$this->_sections['sec1']['index']]; ?>

              </OPTION>
            <?php endfor; endif; ?>
        </SELECT></TD>
    </TR>
    <TR>
      <TD>Date</TD>
      <TD> <?php echo smarty_function_html_select_date(array('prefix' => 'entryDate','start_year' => 1990,'end_year' => 2025,'day_format' => "%02d",'month_format' => "%b",'field_order' => 'DMY'), $this);?>
</TD>
    </TR>
   <TR>
   	<TD>Transaction In</TD>
   	<TD><select name="transMode">
   				<option value="Cash">- Cash -</option>
   				<?php unset($this->_sections['secBank']);
$this->_sections['secBank']['name'] = 'secBank';
$this->_sections['secBank']['loop'] = is_array($_loop=$this->_tpl_vars['bank']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['secBank']['show'] = true;
$this->_sections['secBank']['max'] = $this->_sections['secBank']['loop'];
$this->_sections['secBank']['step'] = 1;
$this->_sections['secBank']['start'] = $this->_sections['secBank']['step'] > 0 ? 0 : $this->_sections['secBank']['loop']-1;
if ($this->_sections['secBank']['show']) {
    $this->_sections['secBank']['total'] = $this->_sections['secBank']['loop'];
    if ($this->_sections['secBank']['total'] == 0)
        $this->_sections['secBank']['show'] = false;
} else
    $this->_sections['secBank']['total'] = 0;
if ($this->_sections['secBank']['show']):

            for ($this->_sections['secBank']['index'] = $this->_sections['secBank']['start'], $this->_sections['secBank']['iteration'] = 1;
                 $this->_sections['secBank']['iteration'] <= $this->_sections['secBank']['total'];
                 $this->_sections['secBank']['index'] += $this->_sections['secBank']['step'], $this->_sections['secBank']['iteration']++):
$this->_sections['secBank']['rownum'] = $this->_sections['secBank']['iteration'];
$this->_sections['secBank']['index_prev'] = $this->_sections['secBank']['index'] - $this->_sections['secBank']['step'];
$this->_sections['secBank']['index_next'] = $this->_sections['secBank']['index'] + $this->_sections['secBank']['step'];
$this->_sections['secBank']['first']      = ($this->_sections['secBank']['iteration'] == 1);
$this->_sections['secBank']['last']       = ($this->_sections['secBank']['iteration'] == $this->_sections['secBank']['total']);
?>
   				    <OPTION value="<?php echo $this->_tpl_vars['bank'][$this->_sections['secBank']['index']]; ?>
">
                <?php echo $this->_tpl_vars['bank'][$this->_sections['secBank']['index']]; ?>

              </OPTION>
   				<?php endfor; endif; ?>
   		</select></Td>
   </TR>
   <TR>
     <TD><INPUT type="radio" name="r1" value="d" checked>Credit
         &nbsp;&nbsp;
         <INPUT type="radio" name="r1" value="w">Debit</TD>
     <TD><INPUT type="text" name="txtdwAmount"></TD>
   </TR>
   <TR>
     <TD>Transaction Type : </TD>
     <TD>
         <INPUT type="radio" name="transType" value="Other" checked>Other
         &nbsp;&nbsp;&nbsp;
         <INPUT type="radio" name="transType" value="Margin">Margin
     </TD>
   </TR>
   <TR>
   	<TD>Exchange</TD>
   	<TD><select name="exchange">
   				<?php unset($this->_sections['secExchange']);
$this->_sections['secExchange']['name'] = 'secExchange';
$this->_sections['secExchange']['loop'] = is_array($_loop=$this->_tpl_vars['exchangeArray']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['secExchange']['show'] = true;
$this->_sections['secExchange']['max'] = $this->_sections['secExchange']['loop'];
$this->_sections['secExchange']['step'] = 1;
$this->_sections['secExchange']['start'] = $this->_sections['secExchange']['step'] > 0 ? 0 : $this->_sections['secExchange']['loop']-1;
if ($this->_sections['secExchange']['show']) {
    $this->_sections['secExchange']['total'] = $this->_sections['secExchange']['loop'];
    if ($this->_sections['secExchange']['total'] == 0)
        $this->_sections['secExchange']['show'] = false;
} else
    $this->_sections['secExchange']['total'] = 0;
if ($this->_sections['secExchange']['show']):

            for ($this->_sections['secExchange']['index'] = $this->_sections['secExchange']['start'], $this->_sections['secExchange']['iteration'] = 1;
                 $this->_sections['secExchange']['iteration'] <= $this->_sections['secExchange']['total'];
                 $this->_sections['secExchange']['index'] += $this->_sections['secExchange']['step'], $this->_sections['secExchange']['iteration']++):
$this->_sections['secExchange']['rownum'] = $this->_sections['secExchange']['iteration'];
$this->_sections['secExchange']['index_prev'] = $this->_sections['secExchange']['index'] - $this->_sections['secExchange']['step'];
$this->_sections['secExchange']['index_next'] = $this->_sections['secExchange']['index'] + $this->_sections['secExchange']['step'];
$this->_sections['secExchange']['first']      = ($this->_sections['secExchange']['iteration'] == 1);
$this->_sections['secExchange']['last']       = ($this->_sections['secExchange']['iteration'] == $this->_sections['secExchange']['total']);
?>
   				    <OPTION value="<?php echo $this->_tpl_vars['exchangeArray'][$this->_sections['secExchange']['index']]; ?>
">
                <?php echo $this->_tpl_vars['exchangeArray'][$this->_sections['secExchange']['index']]; ?>

              </OPTION>
   				<?php endfor; endif; ?>
   		</select></Td>
   </TR>
   <TR>
     <TD>Note : </TD>
     <TD><INPUT type="text" name="itemIdExpiryDate" size="50"></TD>
   </TR>
   <TR>
      <TD><INPUT type=submit value="Submit Me">
         <INPUT type=reset value="Reset"></TD>
   </TR>
</TABLE>
</FORM>
</BODY>
</HTML>