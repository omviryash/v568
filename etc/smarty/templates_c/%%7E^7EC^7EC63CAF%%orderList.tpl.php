<?php /* Smarty version 2.6.10, created on 2014-02-11 04:33:46
         compiled from orderList.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'orderList.tpl', 58, false),array('function', 'html_select_date', 'orderList.tpl', 127, false),array('function', 'math', 'orderList.tpl', 235, false),)), $this); ?>
<HTML>
<HEAD><TITLE>Order List</TITLE>
<?php echo '
<SCRIPT language = "javascript">
function conf(orderId)
{
	if(confirm("Are You Sure You want to Delete Record?"))
			location.href="deleteOrder.php?orderId="+orderId;
	else
  		location.href="orderList.php";
}

function cancelOrder(orderId)
{
	if(confirm("Are You Sure You want to Cancel Order?"))
			location.href="cancelOrder.php?orderId="+orderId;
}

function writeOrder(orderId)
{
	if(confirm("Are You Sure You want to Pass Order?"))
			location.href="writeOrderToTradetxt.php?orderId="+orderId;
}

function change()
 {
  var select1value= document.form1.cboItem.value;
  var select2value=document.form1.cboExpiryDate;
  select2value.options.length=0;
  '; ?>

   <?php unset($this->_sections['sec1']);
$this->_sections['sec1']['name'] = 'sec1';
$this->_sections['sec1']['loop'] = is_array($_loop=$this->_tpl_vars['k']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec1']['show'] = true;
$this->_sections['sec1']['max'] = $this->_sections['sec1']['loop'];
$this->_sections['sec1']['step'] = 1;
$this->_sections['sec1']['start'] = $this->_sections['sec1']['step'] > 0 ? 0 : $this->_sections['sec1']['loop']-1;
if ($this->_sections['sec1']['show']) {
    $this->_sections['sec1']['total'] = $this->_sections['sec1']['loop'];
    if ($this->_sections['sec1']['total'] == 0)
        $this->_sections['sec1']['show'] = false;
} else
    $this->_sections['sec1']['total'] = 0;
if ($this->_sections['sec1']['show']):

            for ($this->_sections['sec1']['index'] = $this->_sections['sec1']['start'], $this->_sections['sec1']['iteration'] = 1;
                 $this->_sections['sec1']['iteration'] <= $this->_sections['sec1']['total'];
                 $this->_sections['sec1']['index'] += $this->_sections['sec1']['step'], $this->_sections['sec1']['iteration']++):
$this->_sections['sec1']['rownum'] = $this->_sections['sec1']['iteration'];
$this->_sections['sec1']['index_prev'] = $this->_sections['sec1']['index'] - $this->_sections['sec1']['step'];
$this->_sections['sec1']['index_next'] = $this->_sections['sec1']['index'] + $this->_sections['sec1']['step'];
$this->_sections['sec1']['first']      = ($this->_sections['sec1']['iteration'] == 1);
$this->_sections['sec1']['last']       = ($this->_sections['sec1']['iteration'] == $this->_sections['sec1']['total']);
?>
    if( select1value=="<?php echo $this->_tpl_vars['itemId'][$this->_sections['sec1']['index']]; ?>
")
  	<?php echo '{'; ?>

  		<?php unset($this->_sections['sec2']);
$this->_sections['sec2']['name'] = 'sec2';
$this->_sections['sec2']['loop'] = is_array($_loop=$this->_tpl_vars['l']+10) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec2']['show'] = true;
$this->_sections['sec2']['max'] = $this->_sections['sec2']['loop'];
$this->_sections['sec2']['step'] = 1;
$this->_sections['sec2']['start'] = $this->_sections['sec2']['step'] > 0 ? 0 : $this->_sections['sec2']['loop']-1;
if ($this->_sections['sec2']['show']) {
    $this->_sections['sec2']['total'] = $this->_sections['sec2']['loop'];
    if ($this->_sections['sec2']['total'] == 0)
        $this->_sections['sec2']['show'] = false;
} else
    $this->_sections['sec2']['total'] = 0;
if ($this->_sections['sec2']['show']):

            for ($this->_sections['sec2']['index'] = $this->_sections['sec2']['start'], $this->_sections['sec2']['iteration'] = 1;
                 $this->_sections['sec2']['iteration'] <= $this->_sections['sec2']['total'];
                 $this->_sections['sec2']['index'] += $this->_sections['sec2']['step'], $this->_sections['sec2']['iteration']++):
$this->_sections['sec2']['rownum'] = $this->_sections['sec2']['iteration'];
$this->_sections['sec2']['index_prev'] = $this->_sections['sec2']['index'] - $this->_sections['sec2']['step'];
$this->_sections['sec2']['index_next'] = $this->_sections['sec2']['index'] + $this->_sections['sec2']['step'];
$this->_sections['sec2']['first']      = ($this->_sections['sec2']['iteration'] == 1);
$this->_sections['sec2']['last']       = ($this->_sections['sec2']['iteration'] == $this->_sections['sec2']['total']);
?>
        <?php if ($this->_tpl_vars['expiryDate'][$this->_sections['sec1']['index']][$this->_sections['sec2']['index']] != ""): ?>
          select2value.options[<?php echo $this->_sections['sec2']['index']; ?>
]=new Option("<?php echo $this->_tpl_vars['expiryDate'][$this->_sections['sec1']['index']][$this->_sections['sec2']['index']]; ?>
","<?php echo $this->_tpl_vars['expiryDate'][$this->_sections['sec1']['index']][$this->_sections['sec2']['index']]; ?>
"); 
        <?php endif; ?>
        <?php if ($this->_tpl_vars['expiryDate'][$this->_sections['sec1']['index']][$this->_sections['sec2']['index']] == ($this->_tpl_vars['expiryDateBack'])): ?>
          select2value.options[<?php echo $this->_sections['sec2']['index']; ?>
].selected=true; 
        <?php endif; ?>
      <?php endfor; endif; ?>
    <?php echo '}'; ?>

  <?php endfor; endif; ?>
  }
</SCRIPT>
</HEAD>
<?php if ($this->_tpl_vars['listOnly'] == '0'): ?>
<BODY onLoad="change();">
<?php else: ?>
<BODY onKeyPress="if(event.keyCode==27)  window.close();">
<?php endif; ?>
  <FORM name="form1" action="" method="POST">
<?php if ($this->_tpl_vars['listOnly'] == '0'): ?>
<A href="./index.php">Home</A><BR><BR>
  Client:
  <SELECT name="cboClientName">
    <option value="All">All</option>
    <?php echo smarty_function_html_options(array('values' => ($this->_tpl_vars['clientId']),'output' => ($this->_tpl_vars['clientName']),'selected' => ($this->_tpl_vars['clientNameBack'])), $this);?>

  </SELECT>
  Item:
  <SELECT name="cboItem" onChange="change();">
    <option value="All">All</option>
    <?php echo smarty_function_html_options(array('values' => ($this->_tpl_vars['itemId']),'output' => ($this->_tpl_vars['itemId']),'selected' => ($this->_tpl_vars['itemBack'])), $this);?>

  </SELECT>
  Expiry Date:
  <SELECT name="cboExpiryDate">
    <option value="All">All</option>
  </SELECT>
  Order Status:
  <SELECT name="cboStatus">
    <option value="All">All</option>
    <?php if ($this->_tpl_vars['orderStatusBack'] == 'Pending'): ?>
      <option value="Pending" selected=selected>Pending</option>
    <?php else: ?>
      <option value="Pending">Pending</option>
    <?php endif; ?>

    <?php if ($this->_tpl_vars['orderStatusBack'] == 'Cancel'): ?>
      <option value="Cancel" selected=selected>Cancel</option>
    <?php else: ?>
      <option value="Cancel">Cancel</option>
    <?php endif; ?>

    <?php if ($this->_tpl_vars['orderStatusBack'] == 'Executed'): ?>
      <option value="Executed" selected=selected>Executed</option>
    <?php else: ?>
      <option value="Executed">Executed</option>
    <?php endif; ?>

  </SELECT><BR>
  Order Type:
  <SELECT name="cboOrderType">
    <option value="All">All</option>
    <?php if ($this->_tpl_vars['orderTypeBack'] == 'RL'): ?>
      <option value="RL" selected=selected>RL</option>
    <?php else: ?>
      <option value="RL">RL</option>
    <?php endif; ?>
    
    <?php if ($this->_tpl_vars['orderTypeBack'] == 'SL'): ?>
      <option value="SL" selected=selected>SL</option>
    <?php else: ?>
      <option value="SL">SL</option>
    <?php endif; ?>
  </SELECT>
  Order Validity:
  <SELECT name="cboOrderValidity">
    <option value="All">All</option>
    <?php if ($this->_tpl_vars['orderValidityBack'] == 'EOS'): ?>
      <option value="EOS" selected=selected>EOS</option>
    <?php else: ?>
      <option value="EOS">EOS</option>
    <?php endif; ?>
    
    <?php if ($this->_tpl_vars['orderValidityBack'] == 'GTD'): ?>
      <option value="GTD" selected=selected>GTD</option>
    <?php else: ?>
      <option value="GTD">GTD</option>
    <?php endif; ?>
    
    <?php if ($this->_tpl_vars['orderValidityBack'] == 'GTC'): ?>
      <option value="GTC" selected=selected>GTC</option>
    <?php else: ?>
      <option value="GTC">GTC</option>
    <?php endif; ?>
  </SELECT><BR>
  From: <?php echo smarty_function_html_select_date(array('prefix' => 'toDate','time' => $this->_tpl_vars['dateTo'],'start_year' => "+1",'end_year' => "-1",'day_value_format' => "%02d",'field_order' => 'dmy'), $this);?>

  To: <?php echo smarty_function_html_select_date(array('prefix' => 'fromDate','time' => $this->_tpl_vars['dateFrom'],'start_year' => "+1",'end_year' => "-1",'day_value_format' => "%02d",'field_order' => 'dmy'), $this);?>

  <INPUT type="submit" name="btnSubmit" value="Go!!"><BR><BR>
<?php endif; ?>
  <HR>
  
  <TABLE border="1" cellSpacing="0" cellPadding="1">
    <!--<TR>
      <TH colspan="13">ORDER LIST</TH>
    </TR>-->
    <TR style="font-size: 14;" align="center" bgcolor="pink">
      <TD>Delete</TD>
      <TD>Cancel</TD>
      <TD>Date</TD>
      <TD>Name</TD>
      <TD>Item</TD>
      <TD>Expiry Date</TD>
      <TD>B/S</TD>
      <TD>Qty</TD>
      <TD>Price</TD>
      <TD>Trig</TD>
      <TD>Status</TD>
      <TD>Order Type</TD>
      <TD>Validity</TD>
      <TD>Remarks</TD>
      <TD>Vendor</TD>
      <TD>Valid Till Date</TD>
    </TR>
    <?php if ($this->_tpl_vars['i'] != '0'): ?>
    <?php unset($this->_sections['sec1']);
$this->_sections['sec1']['name'] = 'sec1';
$this->_sections['sec1']['loop'] = is_array($_loop=$this->_tpl_vars['i']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec1']['show'] = true;
$this->_sections['sec1']['max'] = $this->_sections['sec1']['loop'];
$this->_sections['sec1']['step'] = 1;
$this->_sections['sec1']['start'] = $this->_sections['sec1']['step'] > 0 ? 0 : $this->_sections['sec1']['loop']-1;
if ($this->_sections['sec1']['show']) {
    $this->_sections['sec1']['total'] = $this->_sections['sec1']['loop'];
    if ($this->_sections['sec1']['total'] == 0)
        $this->_sections['sec1']['show'] = false;
} else
    $this->_sections['sec1']['total'] = 0;
if ($this->_sections['sec1']['show']):

            for ($this->_sections['sec1']['index'] = $this->_sections['sec1']['start'], $this->_sections['sec1']['iteration'] = 1;
                 $this->_sections['sec1']['iteration'] <= $this->_sections['sec1']['total'];
                 $this->_sections['sec1']['index'] += $this->_sections['sec1']['step'], $this->_sections['sec1']['iteration']++):
$this->_sections['sec1']['rownum'] = $this->_sections['sec1']['iteration'];
$this->_sections['sec1']['index_prev'] = $this->_sections['sec1']['index'] - $this->_sections['sec1']['step'];
$this->_sections['sec1']['index_next'] = $this->_sections['sec1']['index'] + $this->_sections['sec1']['step'];
$this->_sections['sec1']['first']      = ($this->_sections['sec1']['iteration'] == 1);
$this->_sections['sec1']['last']       = ($this->_sections['sec1']['iteration'] == $this->_sections['sec1']['total']);
?>
      <?php if ($this->_tpl_vars['buySell'][$this->_sections['sec1']['index']] == 'Buy'): ?>
        <TR style="color:blue">
      <?php else: ?>
        <TR style="color:red">
      <?php endif; ?>
      <TD><A href="javascript: conf(<?php echo $this->_tpl_vars['orderId'][$this->_sections['sec1']['index']]; ?>
);">Delete</A></TD>
      <?php if ($this->_tpl_vars['orderStatus'][$this->_sections['sec1']['index']] != 'Executed' && $this->_tpl_vars['orderStatus'][$this->_sections['sec1']['index']] != 'Cancel'): ?>
        <TD><A href="javascript: cancelOrder(<?php echo $this->_tpl_vars['orderId'][$this->_sections['sec1']['index']]; ?>
);">Cancel</A></TD>
        <TD><A href="javascript: writeOrder(<?php echo $this->_tpl_vars['orderId'][$this->_sections['sec1']['index']]; ?>
);"><?php echo $this->_tpl_vars['orderDate'][$this->_sections['sec1']['index']]; ?>
</A> <?php echo $this->_tpl_vars['orderTime'][$this->_sections['sec1']['index']]; ?>
</TD>
      <?php else: ?>
        <TD align="center">-</TD>
        <TD NOWRAP><?php echo $this->_tpl_vars['orderDate'][$this->_sections['sec1']['index']]; ?>
 <?php echo $this->_tpl_vars['orderTime'][$this->_sections['sec1']['index']]; ?>
</TD>
      <?php endif; ?>
      
      <TD align="center"><?php echo $this->_tpl_vars['clientNameSelected'][$this->_sections['sec1']['index']]; ?>
</TD>
      <TD align="center"><?php echo $this->_tpl_vars['itemSelected'][$this->_sections['sec1']['index']]; ?>
</TD>
      <TD><?php echo $this->_tpl_vars['expiryDateSelected'][$this->_sections['sec1']['index']]; ?>
</TD>
      <TD><?php echo $this->_tpl_vars['buySell'][$this->_sections['sec1']['index']]; ?>
</TD>
      <TD ALIGN="RIGHT"><?php echo $this->_tpl_vars['qty'][$this->_sections['sec1']['index']]; ?>
</TD>
      <TD ALIGN="RIGHT">
        <?php if ($this->_tpl_vars['orderStatus'][$this->_sections['sec1']['index']] == 'Pending'): ?>
        <A href='' onClick="tradeWindow=window.open('tradeEdit.php?orderId=<?php echo $this->_tpl_vars['orderId'][$this->_sections['sec1']['index']]; ?>
', 'tradeWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=130, left=1, top=300'); return false;">
        <?php endif; ?>
          <?php echo $this->_tpl_vars['price'][$this->_sections['sec1']['index']]; ?>

         <?php if ($this->_tpl_vars['orderStatus'][$this->_sections['sec1']['index']] == 'Pending'): ?>
         </A>
         <?php endif; ?>
      </TD>
      <TD align="center"><?php echo $this->_tpl_vars['triggerPrice'][$this->_sections['sec1']['index']]; ?>
</TD>
      <TD align="center"><?php echo $this->_tpl_vars['orderStatus'][$this->_sections['sec1']['index']]; ?>
</TD>
      <TD align="center"><?php echo $this->_tpl_vars['orderType'][$this->_sections['sec1']['index']]; ?>
</TD>
      <TD align="center"><?php echo $this->_tpl_vars['orderValidity'][$this->_sections['sec1']['index']]; ?>
</TD>
      <TD><?php echo $this->_tpl_vars['userRemarks'][$this->_sections['sec1']['index']]; ?>
</TD>
      <TD><?php echo $this->_tpl_vars['vendor'][$this->_sections['sec1']['index']]; ?>
</TD>
      <TD><?php echo $this->_tpl_vars['orderValidTillDate'][$this->_sections['sec1']['index']]; ?>
</TD>
    </TR>
    <?php endfor; endif; ?>
    <?php endif; ?>
    
    <?php if ($this->_tpl_vars['message'] == 'No Records Found'): ?>
    <TR>
      <TH colspan="15"><?php echo $this->_tpl_vars['message']; ?>
</TH>
    </TR>
    <?php else: ?>
    <TR>
      <TD colspan="15">Total Rows Selected = <?php echo $this->_sections['sec1']['index']; ?>
</TD>
    </TR>
    <?php endif; ?>
  </TABLE>
<TABLE border="1" cellPadding="0" cellSpacing="0" width="100%">
<TR bgcolor="pink" align="center">
<TD>Item</TD>
<TD NOWRAP>Expiry Date</TD>
<TD>Pending</TD>
<TD>Cancel</TD>
<TD NOWRAP>Executed</TD>
<TD NOWRAP>BQty</TD>
<TD NOWRAP>SQty</TD>
<TD NOWRAP>NetQty</TD>
<TD NOWRAP>AvgB Price</TD>
<TD NOWRAP>AvgS Price</TD>
<TD>RL</TD>
<TD>SL</TD>
<TD>EOS</TD>
<TD>GTD</TD>
<TD>GTC</TD>
</TR>

  <?php unset($this->_sections['sec4']);
$this->_sections['sec4']['name'] = 'sec4';
$this->_sections['sec4']['loop'] = is_array($_loop=$this->_tpl_vars['num1']+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec4']['show'] = true;
$this->_sections['sec4']['max'] = $this->_sections['sec4']['loop'];
$this->_sections['sec4']['step'] = 1;
$this->_sections['sec4']['start'] = $this->_sections['sec4']['step'] > 0 ? 0 : $this->_sections['sec4']['loop']-1;
if ($this->_sections['sec4']['show']) {
    $this->_sections['sec4']['total'] = $this->_sections['sec4']['loop'];
    if ($this->_sections['sec4']['total'] == 0)
        $this->_sections['sec4']['show'] = false;
} else
    $this->_sections['sec4']['total'] = 0;
if ($this->_sections['sec4']['show']):

            for ($this->_sections['sec4']['index'] = $this->_sections['sec4']['start'], $this->_sections['sec4']['iteration'] = 1;
                 $this->_sections['sec4']['iteration'] <= $this->_sections['sec4']['total'];
                 $this->_sections['sec4']['index'] += $this->_sections['sec4']['step'], $this->_sections['sec4']['iteration']++):
$this->_sections['sec4']['rownum'] = $this->_sections['sec4']['iteration'];
$this->_sections['sec4']['index_prev'] = $this->_sections['sec4']['index'] - $this->_sections['sec4']['step'];
$this->_sections['sec4']['index_next'] = $this->_sections['sec4']['index'] + $this->_sections['sec4']['step'];
$this->_sections['sec4']['first']      = ($this->_sections['sec4']['iteration'] == 1);
$this->_sections['sec4']['last']       = ($this->_sections['sec4']['iteration'] == $this->_sections['sec4']['total']);
?>
    <TR>
    <TD><?php echo $this->_tpl_vars['rowItem'][$this->_sections['sec4']['index']]; ?>
</TD>
    <TD><?php echo $this->_tpl_vars['rowExpiryDate'][$this->_sections['sec4']['index']]; ?>
</TD>
    <TD ALIGN="RIGHT"><?php echo $this->_tpl_vars['pending'][$this->_sections['sec4']['index']]; ?>
</TD>
    <TD ALIGN="RIGHT"><?php echo $this->_tpl_vars['canceled'][$this->_sections['sec4']['index']]; ?>
</TD>
    <TD ALIGN="RIGHT"><?php echo $this->_tpl_vars['executed'][$this->_sections['sec4']['index']]; ?>
</TD>
    <TD ALIGN="RIGHT"><?php echo $this->_tpl_vars['buyQty'][$this->_sections['sec4']['index']]; ?>
</TD>
    <TD ALIGN="RIGHT"><?php echo $this->_tpl_vars['sellQty'][$this->_sections['sec4']['index']]; ?>
</TD>
    <TD ALIGN="RIGHT"><?php echo smarty_function_math(array('equation' => "(x-y)",'x' => $this->_tpl_vars['buyQty'][$this->_sections['sec4']['index']],'y' => $this->_tpl_vars['sellQty'][$this->_sections['sec4']['index']]), $this);?>
</TD>
    <TD ALIGN="RIGHT">-</TD>
    <TD ALIGN="RIGHT">-</TD>
    <TD ALIGN="RIGHT"><?php echo $this->_tpl_vars['rl'][$this->_sections['sec4']['index']]; ?>
</TD>
    <TD ALIGN="RIGHT"><?php echo $this->_tpl_vars['sl'][$this->_sections['sec4']['index']]; ?>
</TD>
    <TD ALIGN="RIGHT"><?php echo $this->_tpl_vars['eos'][$this->_sections['sec4']['index']]; ?>
</TD>
    <TD ALIGN="RIGHT"><?php echo $this->_tpl_vars['gtd'][$this->_sections['sec4']['index']]; ?>
</TD>
    <TD ALIGN="RIGHT"><?php echo $this->_tpl_vars['gtc'][$this->_sections['sec4']['index']]; ?>
</TD>
</TR>    
  <?php endfor; endif; ?>
</TABLE>
</FORM>
</BODY>
</HTML>