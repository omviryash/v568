<?php /* Smarty version 2.6.10, created on 2014-02-03 07:31:02
         compiled from accTransList.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'accTransList.tpl', 10, false),)), $this); ?>
<BODY>
<A href="./accTransOtherAdd.php">Add</A>&nbsp;&nbsp;
<A href="./mnuAccount.php">Menu</A><BR><BR>
<FORM name="Form1" action="<?php echo $this->_tpl_vars['PHP_SELF']; ?>
" method=POST>
<TABLE border="0" cellPadding="2" cellSpacing="0">
<TR>
  <TD>
      Exchange :
      <select name="exchange" onChange="document.Form1.submit();">
      	<?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['exchange']['exchange'],'output' => $this->_tpl_vars['exchange']['exchange'],'selected' => $this->_tpl_vars['selectedExchange']), $this);?>

      </select>
  </TD>
  <TD>
      Name : <SELECT name=cboName onChange="document.Form1.submit();">
            <?php unset($this->_sections['sec1']);
$this->_sections['sec1']['name'] = 'sec1';
$this->_sections['sec1']['loop'] = is_array($_loop=$this->_tpl_vars['i']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec1']['show'] = true;
$this->_sections['sec1']['max'] = $this->_sections['sec1']['loop'];
$this->_sections['sec1']['step'] = 1;
$this->_sections['sec1']['start'] = $this->_sections['sec1']['step'] > 0 ? 0 : $this->_sections['sec1']['loop']-1;
if ($this->_sections['sec1']['show']) {
    $this->_sections['sec1']['total'] = $this->_sections['sec1']['loop'];
    if ($this->_sections['sec1']['total'] == 0)
        $this->_sections['sec1']['show'] = false;
} else
    $this->_sections['sec1']['total'] = 0;
if ($this->_sections['sec1']['show']):

            for ($this->_sections['sec1']['index'] = $this->_sections['sec1']['start'], $this->_sections['sec1']['iteration'] = 1;
                 $this->_sections['sec1']['iteration'] <= $this->_sections['sec1']['total'];
                 $this->_sections['sec1']['index'] += $this->_sections['sec1']['step'], $this->_sections['sec1']['iteration']++):
$this->_sections['sec1']['rownum'] = $this->_sections['sec1']['iteration'];
$this->_sections['sec1']['index_prev'] = $this->_sections['sec1']['index'] - $this->_sections['sec1']['step'];
$this->_sections['sec1']['index_next'] = $this->_sections['sec1']['index'] + $this->_sections['sec1']['step'];
$this->_sections['sec1']['first']      = ($this->_sections['sec1']['iteration'] == 1);
$this->_sections['sec1']['last']       = ($this->_sections['sec1']['iteration'] == $this->_sections['sec1']['total']);
?>
            <OPTION value=<?php echo $this->_tpl_vars['resuId'][$this->_sections['sec1']['index']]; ?>
 <?php echo $this->_tpl_vars['SELECTED'][$this->_sections['sec1']['index']]; ?>
>
            <?php echo $this->_tpl_vars['resuName'][$this->_sections['sec1']['index']]; ?>

            </OPTION>
            <?php endfor; endif; ?>
            </SELECT>
  </TD>
 	<TD>&nbsp;&nbsp;Transaction In&nbsp;</TD>
 	<TD><select name="transModeOpt" onChange="document.Form1.submit();">
        <OPTION value='All' <?php echo $this->_tpl_vars['all']; ?>
>All </OPTION>
        <OPTION value='Cash' <?php echo $this->_tpl_vars['cash']; ?>
>Cash </OPTION>
        <?php unset($this->_sections['sec']);
$this->_sections['sec']['name'] = 'sec';
$this->_sections['sec']['loop'] = is_array($_loop=$this->_tpl_vars['b']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec']['show'] = true;
$this->_sections['sec']['max'] = $this->_sections['sec']['loop'];
$this->_sections['sec']['step'] = 1;
$this->_sections['sec']['start'] = $this->_sections['sec']['step'] > 0 ? 0 : $this->_sections['sec']['loop']-1;
if ($this->_sections['sec']['show']) {
    $this->_sections['sec']['total'] = $this->_sections['sec']['loop'];
    if ($this->_sections['sec']['total'] == 0)
        $this->_sections['sec']['show'] = false;
} else
    $this->_sections['sec']['total'] = 0;
if ($this->_sections['sec']['show']):

            for ($this->_sections['sec']['index'] = $this->_sections['sec']['start'], $this->_sections['sec']['iteration'] = 1;
                 $this->_sections['sec']['iteration'] <= $this->_sections['sec']['total'];
                 $this->_sections['sec']['index'] += $this->_sections['sec']['step'], $this->_sections['sec']['iteration']++):
$this->_sections['sec']['rownum'] = $this->_sections['sec']['iteration'];
$this->_sections['sec']['index_prev'] = $this->_sections['sec']['index'] - $this->_sections['sec']['step'];
$this->_sections['sec']['index_next'] = $this->_sections['sec']['index'] + $this->_sections['sec']['step'];
$this->_sections['sec']['first']      = ($this->_sections['sec']['iteration'] == 1);
$this->_sections['sec']['last']       = ($this->_sections['sec']['iteration'] == $this->_sections['sec']['total']);
?>
        <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['bankId'][$this->_sections['sec']['index']],'output' => $this->_tpl_vars['bank'][$this->_sections['sec']['index']],'selected' => $this->_tpl_vars['selectBankId']), $this);?>

        <!--<OPTION value=<?php echo $this->_tpl_vars['bankId'][$this->_sections['sec']['index']]; ?>
 <?php echo $this->_tpl_vars['selectBankId']; ?>
>
        <?php echo $this->_tpl_vars['bank'][$this->_sections['sec']['index']]; ?>

        </OPTION>-->
        <?php endfor; endif; ?>
    </select>
  </Td>
 	<TD>&nbsp;&nbsp;Transaction Type &nbsp;</TD>
 	<TD>
 		<select name="drCr" onChange="document.Form1.submit();">
      <OPTION value='All' <?php echo $this->_tpl_vars['all']; ?>
>All </OPTION>
      <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['drCrValue'],'selected' => $this->_tpl_vars['selectedMod']), $this);?>

 		</select></Td>
  <TD vAlign="top">
    <A href="selectDtSession.php?goTo=accTransList">Date range</A> : <?php echo $this->_tpl_vars['fromDate']; ?>
 To : <?php echo $this->_tpl_vars['toDate']; ?>

  </TD>
</TR>
</TABLE>
</FORM>
  <TABLE BORDER=1 cellspacing="0" cellpadding="3">
  <TR>
    <TH colspan="6">&nbsp;</TH>
    <TH>&nbsp;</TH>
    <TH colspan="3" align="center">Total</TH>
  </TR>
  <TR>
    <TD ALIGN="center">Delete</TD>
    <TD ALIGN="center">
    <B>  Exchange</B>
    </TD>
    <TD ALIGN="center">
    <B>  Date</B>
    </TD>
    <TD align="center">
    <B>Transaction In</B>
    </TD>
    <TD align="center">
    <B>Transaction Type</B>
    </TD>
    <TD ALIGN="center">
    <B> Note </B>
    </TD>
    <?php if ($this->_tpl_vars['selectedMod'] == 'd'): ?>
      <TD ALIGN="center">
        <B>! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Credit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; !</B>
      </TD>
    <?php elseif ($this->_tpl_vars['selectedMod'] == 'w'): ?>
      <TD ALIGN="center" NOWRAP>
        <B>! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Debit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; !</B>
      </TD>
    <?php else: ?>
      <TD ALIGN="center">
        <B>! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Credit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; !</B>
      </TD>
      <TD ALIGN="center" NOWRAP>
        <B>! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Debit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; !</B>
      </TD>
    <?php endif; ?>
    <TD ALIGN="center" >
      <B>&nbsp;</B>
    </TD>
    <TD ALIGN="center">
      <B>Margin Balance</B>
    </TD>
    <TD><B>Total Balance</B></TD>
  </TR>
    <?php unset($this->_sections['sec2']);
$this->_sections['sec2']['name'] = 'sec2';
$this->_sections['sec2']['loop'] = is_array($_loop=$this->_tpl_vars['k']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec2']['show'] = true;
$this->_sections['sec2']['max'] = $this->_sections['sec2']['loop'];
$this->_sections['sec2']['step'] = 1;
$this->_sections['sec2']['start'] = $this->_sections['sec2']['step'] > 0 ? 0 : $this->_sections['sec2']['loop']-1;
if ($this->_sections['sec2']['show']) {
    $this->_sections['sec2']['total'] = $this->_sections['sec2']['loop'];
    if ($this->_sections['sec2']['total'] == 0)
        $this->_sections['sec2']['show'] = false;
} else
    $this->_sections['sec2']['total'] = 0;
if ($this->_sections['sec2']['show']):

            for ($this->_sections['sec2']['index'] = $this->_sections['sec2']['start'], $this->_sections['sec2']['iteration'] = 1;
                 $this->_sections['sec2']['iteration'] <= $this->_sections['sec2']['total'];
                 $this->_sections['sec2']['index'] += $this->_sections['sec2']['step'], $this->_sections['sec2']['iteration']++):
$this->_sections['sec2']['rownum'] = $this->_sections['sec2']['iteration'];
$this->_sections['sec2']['index_prev'] = $this->_sections['sec2']['index'] - $this->_sections['sec2']['step'];
$this->_sections['sec2']['index_next'] = $this->_sections['sec2']['index'] + $this->_sections['sec2']['step'];
$this->_sections['sec2']['first']      = ($this->_sections['sec2']['iteration'] == 1);
$this->_sections['sec2']['last']       = ($this->_sections['sec2']['iteration'] == $this->_sections['sec2']['total']);
?>
      <TR>
        <TD>
          <?php if ($this->_tpl_vars['itemIdExpiryDate'][$this->_sections['sec2']['index']] != 'Opening'): ?>
          <A onclick="return confirm('Are you sure?');" 
          href="accCashFlowDelete.php?cashFlowId=<?php echo $this->_tpl_vars['cashFlowId'][$this->_sections['sec2']['index']]; ?>
">Delete</A>
          <?php else: ?>
          &nbsp;
          <?php endif; ?>
        </TD>
        <TD NOWRAP><?php if ($this->_tpl_vars['itemIdExpiryDate'][$this->_sections['sec2']['index']] != 'Opening'):  echo $this->_tpl_vars['exchangeRoToDisplay'][$this->_sections['sec2']['index']];  endif; ?>&nbsp;</TD>
        <TD NOWRAP><?php echo $this->_tpl_vars['reportDate'][$this->_sections['sec2']['index']]; ?>
</TD>
        <TD NOWRAP><?php echo $this->_tpl_vars['transMode'][$this->_sections['sec2']['index']]; ?>
</TD>
        <TD><?php echo $this->_tpl_vars['transType'][$this->_sections['sec2']['index']]; ?>
</TD>
        <TD NOWRAP><?php echo $this->_tpl_vars['itemIdExpiryDate'][$this->_sections['sec2']['index']]; ?>
&nbsp;</TD>
        <?php if ($this->_tpl_vars['selectedMod'] == 'd'): ?>
          <TD NOWRAP align="right"><?php echo $this->_tpl_vars['reportdwAmount1'][$this->_sections['sec2']['index']]; ?>
</TD>
        <?php elseif ($this->_tpl_vars['selectedMod'] == 'w'): ?>
          <TD NOWRAP align="right"><?php echo $this->_tpl_vars['reportdwAmount2'][$this->_sections['sec2']['index']]; ?>
</TD>
        <?php else: ?>
          <TD NOWRAP align="right"><FONT color="blue"><?php echo $this->_tpl_vars['reportdwAmount1'][$this->_sections['sec2']['index']]; ?>
</FONT></TD>
          <TD NOWRAP align="right"><FONT color="red"><?php echo $this->_tpl_vars['reportdwAmount2'][$this->_sections['sec2']['index']]; ?>
</FONT></TD>
        <?php endif; ?>
        <TD><B>&nbsp;</B></TD>
        <TD NOWRAP align="right"><B><?php echo $this->_tpl_vars['totMargin'][$this->_sections['sec2']['index']]; ?>
</B></TD>
        <?php if ($this->_tpl_vars['reportcurrentBal'][$this->_sections['sec2']['index']] < 0): ?>
          <TD NOWRAP align="right"><B><FONT color="red"><?php echo $this->_tpl_vars['reportcurrentBal'][$this->_sections['sec2']['index']]; ?>
</FONT></B></TD>
        <?php else: ?>
          <TD NOWRAP align="right"><B><FONT color="blue"><?php echo $this->_tpl_vars['reportcurrentBal'][$this->_sections['sec2']['index']]; ?>
</FONT></B></TD>
        <?php endif; ?>
      </TR>
    <?php endfor; endif; ?>
        
  </TABLE>
  <br>
   Margin as on date <?php echo $this->_tpl_vars['toDate']; ?>
 = <B><?php echo $this->_tpl_vars['storeTotMargin']; ?>
</B> 
  <br>
   ---------------------------------------------------------
  <br>
   Balance as on date <?php echo $this->_tpl_vars['toDate']; ?>
 (Margin + Other) = <B><?php echo $this->_tpl_vars['totalBalOnDate']; ?>
</B>
<script type="text/javascript">
	document.Form1.exchange.focus();
	</script>
</BODY>