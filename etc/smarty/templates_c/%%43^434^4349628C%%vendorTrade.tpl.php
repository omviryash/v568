<?php /* Smarty version 2.6.10, created on 2014-11-25 17:08:54
         compiled from vendorTrade.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'vendorTrade.tpl', 81, false),array('function', 'html_select_date', 'vendorTrade.tpl', 85, false),)), $this); ?>
<HTML>
<HEAD><TITLE>Om !!! Add Trade</TITLE>
<SCRIPT language="javascript">
<?php echo '
window.name = \'displayAll\';

function changeName()
{
  document.form1.changedField.value = "vendor";
  document.form1.submit();
}
function changeItem()
{
  document.form1.changedField.value = "itemId";
  document.form1.submit();
}

function changePrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
  if(parseFloat(price) || price==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      price=price+1;
    if(event.keyCode==33)
      price=price+10;
    if(event.keyCode==40)
      price=price-1;
    if(event.keyCode==34)
      price=price-10;
    if(document.form1.price.value != price)
      document.form1.price.value = price;
  }
}
function itemFromPrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
'; ?>

<?php echo $this->_tpl_vars['itemFromPriceJS']; ?>

<?php echo '
}

function changeQty()
{
  '; ?>

  var minQty = <?php echo $this->_tpl_vars['minQty']; ?>
;
  <?php echo '
  var qty;
  qty = parseFloat(document.form1.qty.value);
  if(parseFloat(qty) || qty==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      qty=qty+minQty;
    if(event.keyCode==33)
      qty=qty+minQty*5;
    if(event.keyCode==40)
      qty=qty-minQty;
    if(event.keyCode==34)
      qty=qty-minQty*5;
    if(document.form1.qty.value != qty)
      document.form1.qty.value = qty;
  }
}
'; ?>

</SCRIPT>
</HEAD>
<BODY bgColor="#FFFF80">
  <FORM name="form1" action="<?php echo $this->_tpl_vars['PHP_SELF']; ?>
" METHOD="post">
  <INPUT type="hidden" name="changedField" value="">
  <INPUT type="hidden" name="makeTrade" value="0">
  <INPUT type="hidden" name="firstName" value="<?php echo $this->_tpl_vars['firstName']; ?>
">
  <INPUT type="hidden" name="middleName" value="<?php echo $this->_tpl_vars['middleName']; ?>
">
  <INPUT type="hidden" name="lastName" value="<?php echo $this->_tpl_vars['lastName']; ?>
">
  <INPUT type="hidden" name="forStand" value="<?php echo $this->_tpl_vars['forStand']; ?>
">
  <TABLE BORDER=1 width="100%" cellPadding="2" cellSpacing="0">
  <TR>
    <TD>
      Vendor : <SELECT name="vendor" onChange="changeName();">
      <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['vendorSelected']),'values' => ($this->_tpl_vars['vendorValues']),'output' => ($this->_tpl_vars['vendorOutput'])), $this);?>

      </SELECT>
    </TD>
    <TD>Date : 
      <?php echo smarty_function_html_select_date(array('time' => ($this->_tpl_vars['tradeDateDisplay']),'prefix' => 'trade','start_year' => "-1",'end_year' => "+1",'month_format' => "%m",'field_order' => 'DMY','day_value_format' => "%02d"), $this);?>

    </TD>
    <TD colspan="2" NOWRAP>
      Price : <INPUT size="10" type="text" name="price" value="<?php echo $this->_tpl_vars['lastPrice']; ?>
" onKeydown="changePrice();" onBlur="itemFromPrice();">&nbsp;&nbsp;&nbsp;
    </TD>
  </TR>
  <TR>
    <TD>
      <SELECT name="itemId" onChange="changeItem();">
      <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['itemIdSelected']),'values' => ($this->_tpl_vars['itemIdValues']),'output' => ($this->_tpl_vars['itemIdOutput'])), $this);?>

      </SELECT>
      &nbsp;&nbsp;
      <SELECT name="expiryDate">
      <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['expiryDateSelected']),'values' => ($this->_tpl_vars['expiryDateValues']),'output' => ($this->_tpl_vars['expiryDateOutput'])), $this);?>

      </SELECT>
      &nbsp;&nbsp;&nbsp;
      <SELECT name="buySell">
      <?php echo smarty_function_html_options(array('selected' => 'buySellSelected','values' => ($this->_tpl_vars['buySellValues']),'output' => ($this->_tpl_vars['buySellOutput'])), $this);?>

      </SELECT>
      </TD>
    <TD>
      Quantity : <INPUT type="text" name="qty" value="<?php echo $this->_tpl_vars['minQty']; ?>
" onKeyDown="changeQty();">&nbsp;&nbsp;&nbsp;
    </TD>
    <TD colspan="2">
    &nbsp;
    </TD>
  </TR>
  <TR>
<?php if ($this->_tpl_vars['forStand'] == 1): ?>
    <TD>
      <INPUT type="radio" name="standing" value="-1"> Open Standing
      <INPUT type="radio" name="standing" value="1"> Close Standing
    </TD>
<?php endif; ?>
  </TR>
  <TR>
    <TD colspan="4">
      <INPUT type="submit" name="submitBtn" value="Ok !" onClick="document.form1.makeTrade.value=1;">
    <INPUT type="reset" value="Reset">&nbsp;&nbsp;&nbsp;&nbsp;
       <B><?php echo $this->_tpl_vars['vendorWholeName']; ?>
 : </B>
       Deposit : <?php echo $this->_tpl_vars['deposit']; ?>
&nbsp;&nbsp;&nbsp;&nbsp;
       CurrentBal : <?php echo $this->_tpl_vars['currentBal']; ?>
&nbsp;&nbsp;&nbsp;&nbsp;
       Total : <?php echo $this->_tpl_vars['total']; ?>
&nbsp;&nbsp;&nbsp;&nbsp;
       Phone   : <?php echo $this->_tpl_vars['phone']; ?>
&nbsp;&nbsp;&nbsp;&nbsp;
       Mobile  : <?php echo $this->_tpl_vars['mobile']; ?>
&nbsp;
    </TD>
  </TR>
  <TR>
    <TD colspan="4"><B>Last : </B><?php echo $this->_tpl_vars['lastTradeInfoVar']; ?>

    </TD>
  </TR>
  </TABLE>
  <?php echo $this->_tpl_vars['focusScript']; ?>

  </FORM>
</BODY>
</HTML>