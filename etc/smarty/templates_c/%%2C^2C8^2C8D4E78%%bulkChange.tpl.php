<?php /* Smarty version 2.6.10, created on 2014-02-28 19:29:45
         compiled from bulkChange.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'bulkChange.tpl', 74, false),array('function', 'html_select_date', 'bulkChange.tpl', 91, false),array('modifier', 'date_format', 'bulkChange.tpl', 76, false),array('modifier', 'count', 'bulkChange.tpl', 134, false),)), $this); ?>
<HTML>
<HEAD><TITLE></TITLE>
<script type="text/javascript" src="./js/jquery.js"></script>
<?php echo '
<SCRIPT type="text/javascript">
  function changeExchange(exchange)
  {
    $.ajax(
    {
      type:"POST",
      url:"setOption.php",
      data:
      {
        message  : \'changeExchange\',
        exchange : exchange
      },
      success:function(response)
      {
        $(\'#itemDiv\').html(response);
        $(\'#expiryDiv\').html(\'<SELECT name="expiry"></SELECT>\');
      }
    });
  }
  
  function changeItem(itemVal,exchange)
  {
    $.ajax(
    {
      type:"POST",
      url:"setOption.php",
      data:
      {
        message  : \'changeItem\',
        exchange : exchange,
        itemVal  : itemVal
      },
      success:function(response)
      {
        $(\'#expiryDiv\').html(response);
      }
    });
  }
  $(document).ready(function()
  {
    $(\'#checkAll\').click(function()
    {
      var checkid = document.forTradeId.tradeId;
      var exby = this;
      
      if(typeof(checkid.length) == \'undefined\')
    	  checkid.checked = exby.checked? true:false
    	else
    	{
    	  for (i = 0; i < checkid.length; i++)
    	  {
    	    checkid[i].checked = exby.checked? true:false
    	  }
    	}
    });
  });
    
</SCRIPT>
'; ?>

</HEAD>
<BODY>
<a href="./index.php">Home</a>
<FORM name="formSort" action="" method="post">
<TABLE border="1" width="100%" cellPadding="2" cellSpacing="0">
<TR>
  <TD colspan="8" align="center">
    <B>Client :</B>
    <SELECT name="clientForSort" onChange="document.formSort.submit();">
      <OPTION value="0">All </OPTION>
      <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['clientName']['id'],'output' => $this->_tpl_vars['clientName']['name'],'selected' => $this->_tpl_vars['selectClient']), $this);?>

    </SELECT>&nbsp;&nbsp;
    <A href="selectDtSession.php?goTo=<?php echo $this->_tpl_vars['goTo']; ?>
">Date range</A> : <?php echo ((is_array($_tmp=$_SESSION['fromDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d-%m-%Y") : smarty_modifier_date_format($_tmp, "%d-%m-%Y")); ?>
 To : <?php echo ((is_array($_tmp=$_SESSION['toDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d-%m-%Y") : smarty_modifier_date_format($_tmp, "%d-%m-%Y")); ?>
</CENTER>
  </TD>
</TR>
</TABLE>
</FORM>
<FORM name="forTradeId" action="" method="post">
<TABLE border="1" width="100%" cellPadding="2" cellSpacing="0">
  <TR>
    <TD colspan="8">
      <B>Client :</B>
      <SELECT name="clientIdForTradeId">
        <OPTION value="0">As It Is</OPTION>
        <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['clientName']['id'],'output' => $this->_tpl_vars['clientName']['name']), $this);?>

      </SELECT>&nbsp;&nbsp;
      <B>Date :</B>
      <?php echo smarty_function_html_select_date(array('prefix' => 'changeDate','start_year' => '+0','time' => '0000-00-00','end_year' => '+5','year_empty' => 'Is','month_empty' => 'As','day_empty' => 'It'), $this);?>

      &nbsp;&nbsp;
      <B>Buy/Sell :</B>
      <SELECT name="buySell">
        <OPTION value="0">As It Is</OPTION>
        <OPTION value="Buy">Buy</OPTION>
        <OPTION value="Sell">Sell</OPTION>
      </SELECT>
    </TD>
  </TR>
  <TR>
    <TD colspan="8">
      <B>Exchange :</B>
      <SELECT name="exchange" onchange="changeExchange(this.value)">
        <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['exchange']['exchange'],'output' => $this->_tpl_vars['exchange']['exchange']), $this);?>

      </SELECT>&nbsp;&nbsp;
      <B>Item :</B>
      <DIV id="itemDiv" style="display:inline;">
        <SELECT name="item" onchange="changeItem(this.value,document.forTradeId.exchange.value)">
          <OPTION value="0">As It Is</OPTION>
        </SELECT>
      </DIV>&nbsp;&nbsp;
      <B>Expiry :</B>
      <DIV id="expiryDiv" style="display:inline;">
        <SELECT name="expiry"></SELECT>
      </DIV>&nbsp;&nbsp;
      <B>Price :</B>
      <input type="text" name="price" size="9"/>
    </TD>
  </TR>
  <TR>
    <TD align="center" width="30%">Client</TD>
    <TD align="center" width="30%">Date</TD>
    <TD align="center">Buy/Sell</TD>
    <TD align="center">ItemId</TD>
    <TD align="center">Qty</TD>
    <TD align="center">Price</TD>
    <TD align="center">ExpiryDate</TD>
    <TD align="center">TradeRefNo</TD>
  </TR>
  <TR>
    <TD colspan="8"><INPUT type="checkbox" name="checkAll" id="checkAll" value="checkAll"/>Check/UnCheck All</TD>
  </TR>
  <?php unset($this->_sections['sec']);
$this->_sections['sec']['name'] = 'sec';
$this->_sections['sec']['loop'] = is_array($_loop=count($this->_tpl_vars['client'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec']['show'] = true;
$this->_sections['sec']['max'] = $this->_sections['sec']['loop'];
$this->_sections['sec']['step'] = 1;
$this->_sections['sec']['start'] = $this->_sections['sec']['step'] > 0 ? 0 : $this->_sections['sec']['loop']-1;
if ($this->_sections['sec']['show']) {
    $this->_sections['sec']['total'] = $this->_sections['sec']['loop'];
    if ($this->_sections['sec']['total'] == 0)
        $this->_sections['sec']['show'] = false;
} else
    $this->_sections['sec']['total'] = 0;
if ($this->_sections['sec']['show']):

            for ($this->_sections['sec']['index'] = $this->_sections['sec']['start'], $this->_sections['sec']['iteration'] = 1;
                 $this->_sections['sec']['iteration'] <= $this->_sections['sec']['total'];
                 $this->_sections['sec']['index'] += $this->_sections['sec']['step'], $this->_sections['sec']['iteration']++):
$this->_sections['sec']['rownum'] = $this->_sections['sec']['iteration'];
$this->_sections['sec']['index_prev'] = $this->_sections['sec']['index'] - $this->_sections['sec']['step'];
$this->_sections['sec']['index_next'] = $this->_sections['sec']['index'] + $this->_sections['sec']['step'];
$this->_sections['sec']['first']      = ($this->_sections['sec']['iteration'] == 1);
$this->_sections['sec']['last']       = ($this->_sections['sec']['iteration'] == $this->_sections['sec']['total']);
?>
    <?php if ($this->_tpl_vars['client'][$this->_sections['sec']['index']]['buySell'] == 'Buy'): ?>
      <TR style="color:blue;">
    <?php else: ?>
      <TR style="color:red;">
    <?php endif; ?>
      <TD>
        <INPUT type="checkbox" id="tradeId" name="tradeId[]" value="<?php echo $this->_tpl_vars['client'][$this->_sections['sec']['index']]['id']; ?>
" />
        &nbsp;&nbsp;<INPUT type = "submit" name="submitBtn" value="Go!" />&nbsp;
        <?php echo $this->_tpl_vars['client'][$this->_sections['sec']['index']]['name']; ?>

      </TD>
      <TD><?php echo ((is_array($_tmp=$this->_tpl_vars['client'][$this->_sections['sec']['index']]['dateTime'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d-%m-%Y %I:%M:%S %p") : smarty_modifier_date_format($_tmp, "%d-%m-%Y %I:%M:%S %p")); ?>
</TD>
      <TD><?php echo $this->_tpl_vars['client'][$this->_sections['sec']['index']]['buySell']; ?>
</TD>
      <TD><?php echo $this->_tpl_vars['client'][$this->_sections['sec']['index']]['itemId']; ?>
</TD>
      <TD><A href="qtyDevide.php?tradeIdToDevide=<?php echo $this->_tpl_vars['client'][$this->_sections['sec']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['client'][$this->_sections['sec']['index']]['qty']; ?>
</A></TD>
      <TD><?php echo $this->_tpl_vars['client'][$this->_sections['sec']['index']]['price']; ?>
</TD>
      <TD><?php echo $this->_tpl_vars['client'][$this->_sections['sec']['index']]['expiryDate']; ?>
</TD>
      <TD><?php echo $this->_tpl_vars['client'][$this->_sections['sec']['index']]['tradeRefNo']; ?>
</TD>
    </TR>
  <?php endfor; endif; ?>
</TABLE>
</FORM>
<SCRIPT type="text/javascript">
changeExchange(document.forTradeId.exchange.value);
</SCRIPT>
</BODY>
</HTML>