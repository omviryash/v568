<?php
include "./etc/om_config.inc";
include "./etc/functions.inc";
session_start();
$smarty = new SmartyWWW();
if(!isset($_SESSION['user']))
  header("Location:login.php");
else
{
  if(isset($_POST['submitBtn']))
  {
    $extOfImg = pathinfo($_FILES['fileName']['name'], PATHINFO_EXTENSION );
    $fileName = date("Y_m_d_H_i_s").".csv";
    $target_path = "./bhavcopies/".$fileName;
    if(strtolower($extOfImg) == "csv")
    {
      if(move_uploaded_file($_FILES['fileName']['tmp_name'], $target_path)) 
      {
        $handle = fopen($target_path, "r");
        $i = 0;
        while(($data = fgetcsv($handle, 1000, ",")) !== FALSE)
        {
          $csvData[] = $data;
          if($i != 0)
          {
            if($csvData[$i][0] == "FUTIDX" || $csvData[$i][0] == "FUTSTK")
            {
              $standingDtCurrent = DDMMMYYYYToYYYYMMDD($csvData[$i][14]);
              $standingDtNext    = DDMMMYYYYToYYYYMMDDNext($csvData[$i][14]);
              $itemIdExpiryDate  = $csvData[$i][1].substr($csvData[$i][2],0,2).strtoupper(substr($csvData[$i][2],3,3)).substr($csvData[$i][2],7,4);
              $standingPrice     = $csvData[$i][8];
              $insertQuery = "INSERT INTO standing (standingDtCurrent,standingDtNext,
                                                    itemIdExpiryDate,standingPrice,exchange)
                                            VALUES ('".$standingDtCurrent."','".$standingDtNext."',
                                                    '".$itemIdExpiryDate ."',".$standingPrice.",'F_O')";
              $insertQueryResult = mysql_query($insertQuery);
            }
          }
          $i++;
        }
      }
      else
        echo "There was an error uploading the file, please try again!";
    }
    else
      echo "Enter CSV file only";
    
  } 
  $smarty->display('bhavcopyF_O.tpl');
}
?>