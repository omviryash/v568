<?php
  if(isset($_SESSION['userType']) && $_SESSION['userType'] == "client")
    include "./templates/header3client.tpl";
  elseif(isset($_SESSION['userType']) && $_SESSION['userType'] == "operator")
    include "./templates/header3user.tpl";
  else
  {
    if($packfor == 4)
      include "./templates/header4.tpl";
    elseif($packfor == 3)
      include "./templates/header3.tpl";
    elseif($packfor == 2)
      include "./templates/header2.tpl";
    elseif($packfor == 5)
      include "./templates/header5.tpl";
    elseif($packfor == 6)
      include "./templates/header6.tpl";
    elseif($packfor == 7)
      include "./templates/header7.tpl";
    elseif($packfor == 8)
      include "./templates/header8.tpl";
    elseif($packfor == 9)
      include "./templates/header9.tpl";
    elseif($packfor == 10)
      include "./templates/header10.tpl";
    elseif($packfor == 11)
      include "./templates/header11.tpl";
    elseif($packfor == 12)
      include "./templates/header12.tpl";
    else
      include "./templates/header5.tpl";
  }
?>